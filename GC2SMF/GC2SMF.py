#!/usr/bin/python3

############################################################################################
# ebc2dc.py                                                                                #
# >> Convert CSV exports from Garmin Connect to SMF files for Sigma Data Center            #
#  @CyclisteUrbain                                                                         #
#                                                                                          #
#                                                                                          #
#                                                                                          #
#                                                             <!            @              #
#                                                              !___   _____`\<,!_!         #
#                                                              !(*)!--(*)---/(*)           #
############################################################################################

##############################################
# IMPORTS
import argparse
from matplotlib.dates import SEC_PER_DAY
import numpy as np
import pandas as pd
import math
import time
import uuid
from datetime import datetime, timedelta, timezone
from os import path

##############################################
# SETTINGS

SET_OutputDir = "~/Temp/"
SET_Sports = [
    "cycling",
    "bmx",
    "racing_bicycle",
    "mountainbike",
    "triathlon",
    "ebike",
    "indoor_cycling",
    "cyclecross",
    "enduro",
]
SET_DefaultSport = SET_Sports[0]
SET_DefaultBikeNumber = 1

# Colors
class fgc:
    HEADER =  "\033[95m"
    OKBLUE =  "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL =    "\033[91m"
    ENDC =    "\033[0m"
    BOLD =    "\033[1m"

class csv_fields:
    ACTIVITY_TYPE = "Type d'activité"
    DATE             = "Date"
    FAVORITE         = "Favori"
    TITLE            = "Titre"
    DISTANCE         = "Distance"
    CALORIES         = "Calories"
    DURATION         = "Durée"
    DURATION_MOVING  = "Temps de déplacement"
    DURATION_TOTAL   = "Temps écoulé"
    HEARTRATE_AVG    = "Fréquence cardiaque moyenne"
    HEARTRATE_MAX    = "Fréquence cardiaque maximale"
    SPEED_AVG        = "Vitesse moyenne"
    SPEED_MAX        = "Vitesse max"
    CLIMB_TOTAL      = "Ascension totale"
    DESCENT_TOTAL    = "Descente totale"
    ALTITUDE_MIN     = "Altitude minimale"
    ALTITUDE_MAX     = "Altitude maximale"
    CADENCE_AVG      = "Cadence de vélo moyenne"
    CADENCE_MAX      = "Cadence de vélo maximale"
    POWER_AVG        = "Puissance moy."
    POWER_MAX        = "Puissance max."
    CRANK_ROUND      = "Nombre total de coups de pédale"
    TEMPERATURE_MIN  = "Température minimale"
    TEMPERATURE_MAX  = "Température maximale"
    LAP_BEST_TIME    = "Temps du meilleur circuit"
    LAP_COUNT        = "Nombre de tours"

     	 		 	

def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
    return utc + offset


##############################################
# ARGUMENTS CHECK

parser = argparse.ArgumentParser(
    prog="GC2SMF.py",
    description="Translates Garmin Connect CSV exports to Sigma Data Center SMF files",
)
parser.add_argument("--input", "-i", help="path to Garmin Connect CSV file", type=str, default="")
parser.add_argument("--output", "-o", help="path to output directory", type=str, default=SET_OutputDir)
parser.add_argument("--sport","-s",help="trip sport type",type=str,default=SET_DefaultSport,choices=SET_Sports)
parser.add_argument("--bikenumber","-bn",help="bike number (1-3, 0 for no bike)",type=int,default=SET_DefaultBikeNumber)
parser.add_argument("--lines", "-l", help="Number tof lines from the CSV to be imported (0 = all)", type=int, default=0)
parser.add_argument("--favonly", "-f", help="Only activities marked as favorites", action='store_true', default=False)

args = parser.parse_args()

if args.input == "":
    print(fgc.FAIL + "Error : no input file was specified" + fgc.ENDC)
    print(parser.format_help())
    quit()

# Check arguments
filepath = args.input
if not path.isfile(filepath):
    print(fgc.FAIL+ "Error : Input file "+ filepath+ " does not exist or is not a file" + fgc.ENDC)
    print(parser.format_help())
    quit()

outputdir = args.output
if not path.isdir(outputdir):
    print(fgc.FAIL+ "Error : Ouput directory "+ outputdir+ " does not exist or is not a directory"+ fgc.ENDC)
    print(parser.format_help())
    quit()
print(fgc.BOLD + "SMF file will be exported to : " + fgc.ENDC + outputdir)

bikenumber = args.bikenumber
if bikenumber > 3:
    print(
        fgc.FAIL
        + "Error : Bike number must be between 0 and 3, 0 being for no bike"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()

maxlines = args.lines

favorites_only = args.favonly
if(favorites_only and maxlines > 0):
    print(fgc.WARNING + "--lines is ignored when --favonly is used" + fgc.ENDC)
    maxlines = 0

##############################################
# CSV FILE LOAD

#Loading
print(fgc.BOLD + "Loading from CSV Input file : " + fgc.ENDC + filepath)

try:
    garmin_data = pd.read_csv(filepath,sep=",")
    print(fgc.OKGREEN+" Loaded CSV file with "+str(len(garmin_data))+" lines"+fgc.ENDC)
except Exception as oups:
    print(fgc.FAIL+" Could not load CSV : "+format(oups)+fgc.ENDC)

#Filtering if --lines or --favonly was provided
if(favorites_only):
    garmin_data = garmin_data[garmin_data[csv_fields.FAVORITE] == True]
    print("File is filtered to "+str(len(garmin_data))+" favorite(s) line(s)")

if(maxlines > 0):
    garmin_data = garmin_data[:maxlines]
    print("File is filtered to "+str(len(garmin_data))+" line(s)")


##############################################
# CSV FILE ANALYSYS

for line_number in np.arange(0,len(garmin_data),1) :

    print("Traitement de la ligne "+str(line_number)+"/"+str(len(garmin_data)))

    #Titre de l'activité
    activity_title = garmin_data[csv_fields.TITLE][line_number]
    print(fgc.BOLD+ " "+activity_title+ fgc.ENDC)

    #Date/Heure de l'activité
    start_time = datetime.strptime(garmin_data[csv_fields.DATE][line_number],"%Y-%m-%d %H:%M:%S")
    print(fgc.BOLD+ "   Activity Start Time : "+ fgc.ENDC+ datetime.strftime(start_time, "%a %b %d %H:%M:%S %Y"))

    # Durée de l'activitée, hh:mm:ss en secondes
    t = datetime.strptime(garmin_data[csv_fields.DURATION_MOVING][line_number][0:8],"%H:%M:%S")
    duration = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second).total_seconds()
    print(fgc.BOLD+ "   Activity duration : "+ fgc.ENDC+ str(duration)+ " seconds")

    #Temps de pause
    t = datetime.strptime(garmin_data[csv_fields.DURATION_TOTAL][line_number][0:8],"%H:%M:%S")
    temps_pause = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second).total_seconds()-duration
    print(fgc.BOLD+ "   Pause duration : "+ fgc.ENDC+ str(temps_pause)+ " seconds")

    # Distance : km 
    distance = float(garmin_data[csv_fields.DISTANCE][line_number])
    print(fgc.BOLD + "   Activity distance : " + fgc.ENDC + str(distance) + "km")

    # AVERAGE SPEED (km/h)
    #average_speed = float(garmin_data[csv_fields.SPEED_AVG][line_number])
    average_speed = round(distance / (duration/3600),1) 
    print(fgc.BOLD + "   Average speed : " + fgc.ENDC + str(average_speed) + "km/h")

    # MAX SPEED * (km/h)
    max_speed = float(garmin_data[csv_fields.SPEED_MAX][line_number])
    print(fgc.BOLD + "   Activity max speed : " + fgc.ENDC + str(max_speed) + "km/h")

    # Denivelé + (m)
    try:
        altitudedifferencesuphill = int(garmin_data[csv_fields.CLIMB_TOTAL][line_number])
    except:
        altitudedifferencesdownhill = 0
    print(fgc.BOLD + "   Uphill : " + fgc.ENDC + str(altitudedifferencesuphill) + "m")

    # Denivelé - (m)
    try:
        altitudedifferencesdownhill = int(garmin_data[csv_fields.DESCENT_TOTAL][line_number])
    except:
        altitudedifferencesuphill = 0    
    print(fgc.BOLD + "   Downhill : " + fgc.ENDC + str(altitudedifferencesdownhill) + "m")

    # Calories (cal)
    calories = (int((garmin_data[csv_fields.CALORIES][line_number]).replace(",", "")))
    #calories = int(garmin_data[csv_fields.CALORIES][line_number])


    # Frequence cardiaque moyenne (bpm)
    try:
        if(math.isnan(garmin_data[csv_fields.HEARTRATE_AVG][line_number])):
            average_heartbeat_rate = 0
        else:
            average_heartbeat_rate = int(garmin_data[csv_fields.HEARTRATE_AVG][line_number])
    except KeyError:
        average_heartbeat_rate = 0
    print(fgc.BOLD+ "   Avg heart rate : "+ fgc.ENDC+ str(average_heartbeat_rate)+ "bpm")

    # Frequence cardiaque max (bpm)
    try:    
        if(math.isnan(garmin_data[csv_fields.HEARTRATE_MAX][line_number])):
            max_heartbeat_rate = 0
        else:
            max_heartbeat_rate = int(garmin_data[csv_fields.HEARTRATE_MAX][line_number])
    except KeyError:
        max_heartbeat_rate = 0
    print(fgc.BOLD+ "   Max heart rate : "+ fgc.ENDC+ str(max_heartbeat_rate)+ "bpm")

    # AVERAGE CADENCE rpm => rpm
    try:
        average_cadence = int(garmin_data[csv_fields.CADENCE_AVG][line_number])
        print(fgc.BOLD + "   Average cadence : " + fgc.ENDC + str(average_cadence) + "rpm")
    except:
        average_cadence = 0
        print(fgc.BOLD + "   Average cadence : None")

    # MAX CADENCE * max of all max cadence * rpm => rpm
    try:
        max_cadence = int(garmin_data[csv_fields.CADENCE_MAX][line_number])
        print(fgc.BOLD + "   Max cadence : " + fgc.ENDC + str(max_cadence) + "rpm")
    except:
        max_cadence = 0
        print(fgc.BOLD + "   Max cadence : None")
    
    # AVERAGE ALTITUDE * average of altitudes with minutes ponderation * m => m
    average_altitude = 0

    # Altitude max en m
    max_altitude = int((garmin_data[csv_fields.ALTITUDE_MAX][line_number]))
    print(fgc.BOLD + "   Max altitude : " + fgc.ENDC + str(max_altitude) + "m")

    # Altitude min en m
    min_altitude = int((garmin_data[csv_fields.ALTITUDE_MIN][line_number]))
    print(fgc.BOLD + "   Max altitude : " + fgc.ENDC + str(min_altitude) + "m")

    # Puissance Moyenne en w
    try:
        if(math.isnan(garmin_data[csv_fields.POWER_AVG][line_number])):
            average_power = 0
        else:
            average_power = int(garmin_data[csv_fields.POWER_AVG][line_number])
    except KeyError:
        average_power = 0
    print(fgc.BOLD + "   Average power : " + fgc.ENDC + str(average_power) + "W")

    # Puissance Maximale en w
    try:
        if(math.isnan(garmin_data[csv_fields.POWER_MAX][line_number])):
            max_power = 0
        else:
            max_power = int(garmin_data[csv_fields.POWER_MAX][line_number])
    except KeyError:
        max_power = 0
    print(fgc.BOLD + "   Maximum Power : " + fgc.ENDC + str(max_power) + "W")

    #Température Min en °c
    try:
        min_temperature = float(garmin_data[csv_fields.TEMPERATURE_MIN][line_number])
        print(fgc.BOLD + "   Min temperature : " + fgc.ENDC + str(min_temperature) + "°C")
    except ValueError:
        min_temperature=""
        print(fgc.BOLD + "   Min temperature : " + fgc.ENDC + "N/A")


    #Température Max en °c
    try:
        max_temperature = float(garmin_data[csv_fields.TEMPERATURE_MAX][line_number])
        print(fgc.BOLD + "   Max temperature : " + fgc.ENDC + str(max_temperature) + "°C")
    except ValueError:
        max_temperature=""
        print(fgc.BOLD + "   Max temperature : " + fgc.ENDC + "N/A")


    # SPORT (checks and default are aleady handeled by argparse)
    sport = args.sport
    print(fgc.BOLD + "   Sport: " + fgc.ENDC + sport)

    # BIKE NUMBER
    if bikenumber == 0:
        bikenumber_str = "noBike"
    else:
        bikenumber_str = "bike" + str(bikenumber)
    print(fgc.BOLD + "   Bike: " + fgc.ENDC + bikenumber_str)

    # Static pre-set values
    fileDate = datetime.strftime(datetime.now(timezone.utc), "%a %b %d %H:%M:%S %Y")
    guid = str(uuid.uuid4()).upper()
    description = "Export depuis Garmin Connect"

    ############# XML File build ###############

    #General informations and stats
    XMLData = (
        """<?xml version=\"1.0\" encoding=\"utf-8\"?>
    <Activity fileDate=\""""+fileDate+"""\" revision=\"400\">
        <Computer unit="user defined" serial="null" activityType="Cycling" dateCode=""/>
        <GeneralInformation>
            <user color=\"45824\" gender=\"male\"><![CDATA[Standard]]></user>
            <sport><![CDATA["""+sport+"""]]></sport>
            <GUID>"""+guid+"""</GUID>
            <bike>"""+bikenumber_str+"""</bike>
            <name><![CDATA["""+activity_title+"""]]></name>
            <description><![CDATA["""+description+"""]]></description>
            <startDate>"""+datetime.strftime(start_time, "%a %b %d %H:%M:%S %Y")+"""</startDate>
            <distance>"""+ str(distance*1000)+"""</distance>
            <trainingTime>"""+ str(duration*100)+ """</trainingTime>
            <pauseTime>"""+str(temps_pause)+"""</pauseTime>
            <averageSpeed>"""+str(average_speed/3.6)+"""</averageSpeed>
            <maximumSpeed>"""+str(max_speed/3.6)+"""</maximumSpeed>
            <maximumTemperature>"""+str(max_temperature)+"""</maximumTemperature>
            <minimumTemperature>"""+str(min_temperature)+"""</minimumTemperature>""")
    

    #Altitude and climb
    XMLData += (
        """
            <altitudeDifferencesDownhill>"""+str(altitudedifferencesdownhill*1000)+"""</altitudeDifferencesDownhill>
            <altitudeDifferencesUphill>"""+str(altitudedifferencesuphill*1000)+ """</altitudeDifferencesUphill>
            <!-- <averageInclineDownhill>0</averageInclineDownhill> -->
            <!-- <averageInclineUphill>0</averageInclineUphill> -->
            <!-- <averageRiseRateUphill>0</averageRiseRateUphill> -->
            <!-- <averageRiseRateDownhill>0</averageRiseRateDownhill> -->
            <!-- <averageSpeedDownhill>0</averageSpeedDownhill> -->
            <!-- <averageSpeedUphill>0</averageSpeedUphill> -->
            <!-- <distanceDownhill>0</distanceDownhill> -->
            <!-- <distanceUphill>0</distanceUphill> -->
            <maximumAltitude>"""+str(max_altitude*1000)+"""</maximumAltitude>
            <minimumAltitude>"""+str(min_altitude*1000)+"""</minimumAltitude>
            <!-- <maximumInclineDownhill>0</maximumInclineDownhill> -->
            <!-- <maximumInclineUphill>0</maximumInclineUphill> -->
            <!-- <maximumRiseRate>0</maximumRiseRate> -->
            <!-- <minimumRiseRate>0</minimumRiseRate> -->
            <!-- <trainingTimeDownhill>0</trainingTimeDownhill> -->
            <!-- <trainingTimeUphill>0</trainingTimeUphill> -->""")

    #Cadence
    if(average_cadence > 0):
        XMLData += (
        """    
            <averageCadence>"""+str(average_cadence)+"""</averageCadence>""")
    if(max_cadence > 0):
        XMLData += (
        """    
            <maximumCadence>"""+str(max_cadence)+"""</maximumCadence>""")

    #Rythmes cardiaque
    if(average_heartbeat_rate > 0):
        XMLData += (
        """
            <averageHeartrate>"""+str(average_heartbeat_rate)+"""</averageHeartrate>""")
    if(max_heartbeat_rate > 0):
        XMLData += (
        """ 
            <maximumHeartrate>"""+str(max_heartbeat_rate)+"""</maximumHeartrate>""")

    #Puissance
    if(average_power >0):
        XMLData += (
        """
            <averagePower>"""+str(average_power)+"""</averagePower>""")
    if(max_power >0):
        XMLData += (
        """
            <maximumPower>"""+str(max_power)+"""</maximumPower>""")
    
    #Calories
    if(calories > 0):
        XMLData += (
        """
            <calories>"""+str(calories)+"""</calories>""")
    
    #Fin du fichier
    XMLData += (
        """
            <dataType>memory</dataType>
            <externalLink><![CDATA[]]></externalLink>
            <hrMax>0</hrMax>
            <linkedRouteId>0</linkedRouteId>
            <rating>1</rating>
            <feeling>2</feeling>
            <samplingRate>0</samplingRate>
            <statistic>true</statistic>
            <timeOverZone>0</timeOverZone>
            <timeUnderZone>0</timeUnderZone>
            <trackProfile>0</trackProfile>
            <unitId>0</unitId>
            <weather>0</weather>
            <wind>0</wind>
            <zone1Start>0</zone1Start>
            <zone2Start>0</zone2Start>
            <zone3Start>0</zone3Start>
            <zone3End>0</zone3End>
            <activityStatus>none</activityStatus>
            <sharingInfo>{\"sigmaStatisticsId\":\"0\",\"komootId\":\"0\",\"stravaId\":\"0\",\"trainingPeaksId\":\"0\",\"twitterId\":\"0\",\"twoPeaksId\":\"0\",\"facebookId\":\"0\"}</sharingInfo>
            <Participant/>
        </GeneralInformation>
    </Activity>"""
    )


    # Ecriture du XML
    outfilename = "Garmin_Connect-" + datetime.strftime(start_time, "%Y%m%d-%H%M%S") + ".smf"

    with open(outputdir + outfilename, "w") as outfile:
        outfile.write(XMLData)

    print(fgc.OKGREEN+ "\r\nData transfered into file "+ outputdir+outfilename+ fgc.ENDC)
