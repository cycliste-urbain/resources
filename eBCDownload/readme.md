# eBCDownload.py

Downloads Bosch eBike Connect ride data as .json files.
Handles authentication either trough a cookie (see bellow), or, thanks to @eMerzh, through login/password.

## Prerequisites

- Python 3.x
- Modules : sys, os, argparse, requests, datetime, time, json
- a Bosch eBike with a "connected" computer : Kiox (BUI330), Nyon (BUI270, BUI275, BUI350), Kiox 300
- a Bosch eBike Connect account

## Usage

     usage: ebcdownload.py [-h] [--cookie COOKIE] [--output OUTPUT]
                      [--startdate STARTDATE] [--enddate ENDDATE]
                      [--maxtrips MAXTRIPS] [--login LOGIN] [--password PASSWORD]

     optional arguments:
     -h, --help            Show help
     --cookie COOKIE, -c COOKIE
                        Authentication cookie encoded data
     --output OUTPUT, -o OUTPUT
                        Directory where the json files will be created,
                        set to ~/Documents/BoschEBCRides if not specified
     --startdate STARTDATE, -s STARTDATE
                        Start date of range from which the rides are to be
                        dowloaded (dd/mm/yyyy), set to today if not specified
     --enddate ENDDATE, -e ENDDATE
                        End date of range from which the rides are to be
                        dowloaded (dd/mm/yyyy), set to today if not specified
     --maxtrips MAXTRIPS, -m MAXTRIPS
                        maximum trips to be retrieved, set to 2000 if not specified
                        (a trip is a set of n rides, a trip is created each time you
                        reset your bike computer)
      --login LOGIN, -l LOGIN
                            Login for the Bosch eBike Connect Portal
      --password PASSWORD, -p PASSWORD
                            Password for the bosh connect Portal

## How to retreive authentication cookie

In order to transmit authentication data, the authentication cookie has to be passed as an argument to the script.
To retrieve that cookie :

1. Open https://www.ebike-connect.com/dashboard in your browser and log in with your account ticking the "remember me" box
2. Go to _Activities_
3. Open your browser's developer tools and eventually reload the page
4. In the _Network_ pane, filter by type "_XHR_"
5. Select either of the filtered requests with type _vnd.ebike-connect.com_ and go to _Headers_ pane
6. In _Request Headers_, copy the _Cookie_ header's value without the "REMEMBER=" part (If there is no "Remember" cookie, make sure you ticked the box at login)

<img width="1242" alt="get-ebc-cookie" src="https://user-images.githubusercontent.com/108917337/178094782-241a3c57-6ee3-4c18-8135-2fe7ff0b7226.png">

## Known issues

- Python code might not follow best practices as I'm a python newbie
- I'm unaware of the Bosch EBC API limitations, so there might be some throttling on big requests
- Code might contain references to a older version that was relying on a extraction from a HTML file
