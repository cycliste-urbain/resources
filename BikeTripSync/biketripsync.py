#!/usr/bin/python3

#########################################################################################
# biketripsync.py                                                                       #
# >> Exports Garmin Connect activities and Bosch eBike Connect to CSV file and          #
# optionnaly upload them to a One Drive for Business folder                             #
#                                                                                       #
#  @CyclisteUrbain                                                                      #
#                                                             <!            @           #
#                                                              !___   _____`\<,!_!      #
#                                                              !(*)!--(*)---/(*)        #
#########################################################################################

import os
import __main__ as main 
from datetime import datetime, timedelta
from getpass import getpass
from garminconnect import *
import argparse
import requests
import uuid
import syslog
import pandas as pd
import json
from geopy.geocoders import Nominatim
from geopy.distance import geodesic
import msal

# LOADS CONFIGURATION
from bts_config import *

# GETS SCRIPT NAME
script_name = os.path.basename(main.__file__)

#####################################################################################################################################
# GENERAL USE FUNCTIONS AND CLASSES                                                                                                 #
#####################################################################################################################################

##########################################################################
# COLORS : Used for color formating of output
class fgc:
    HEADER  = '\033[95m'
    OKBLUE  = '\033[94m'
    OKGREEN = '\033[92m'
    OKCYAN  = '\033[96m'
    WARNING = '\033[93m'
    FAIL    = '\033[91m'
    ENDC    = '\033[0m'
    BOLD    = '\033[1m'

##########################################################################
# logmessage(message,logfile,level) : Prints a message on stdout and writes it to a log file
def logmessage(message:str,logfile:str=None,level:int=9,usesyslog:bool=False):

    debug_message = False
    
    #Create the logfile if not exists
    if(logfile is None):
        logfile = LOG_PATH+script_name+"-"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".log"
        try:
            with open(logfile, 'w') as lf:
                lf.write(script_name+" : Script startup at "+datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")+"\n")
        except IOError as oups:
            print(fgc.FAIL+"Cannot initiate logfile "+logfile+" : "+format(oups)+fgc.ENDC)

    #Level
    event_time = datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")
    match level:
        case 0:
            event_prefix = event_time+" [VERBOSE] "
            event_color  = fgc.OKBLUE
            syslog_level = syslog.LOG_INFO
        case 1:
            event_prefix = event_time+" [INFO]    "
            event_color  = fgc.OKCYAN
            syslog_level = syslog.LOG_INFO
        case 2:
            event_prefix = event_time+" [SUCCESS] "
            event_color  = fgc.OKGREEN
            syslog_level = syslog.LOG_NOTICE
        case 3:
            event_prefix = event_time+" [WARNING] "
            event_color  = fgc.WARNING
            syslog_level = syslog.LOG_WARNING
        case 4:
            event_prefix = event_time+" [ERROR]   "
            event_color  = fgc.FAIL
            syslog_level = syslog.LOG_ERR
        case 5:
            event_prefix = event_time+" [FTERROR] "
            event_color  = fgc.FAIL+fgc.BOLD
            syslog_level = syslog.LOG_CRIT
        case _:
            event_prefix = event_time+" [DEBUG]   "
            event_color  = ""
            debug_message = True
            syslog_level = syslog.LOG_DEBUG
    
    if debug_mode or debug_message == False :

        #Writes to file
        try:
            with open(logfile, 'a') as lf:
                lf.write(event_prefix+message+"\n")
        except IOError as oups:
            print(fgc.FAIL+"Cannot write to logfile "+logfile+" : "+format(oups)+fgc.ENDC)
        
        #Prints to stdout
        print(event_color+event_prefix+message+fgc.ENDC)

        #Sends to syslog
        if(usesyslog):
            syslog.syslog(syslog_level,message)
    
    #Returns the path to log file
    return logfile

# Returns the City name from latitude and longitude, or None if wasn't found
def get_city_name(latitude, longitude):
    geolocator = Nominatim(user_agent="geoapi")  # Initialize geolocator
    location = geolocator.reverse((latitude, longitude), exactly_one=True)  # Reverse geocoding
    
    city_name = location.raw.get("address", {}).get("city") 

    if city_name is None:
        city_name = location.raw.get("address", {}).get("town") 
    
    return city_name

# Returns the farthest point in a set of coordinates
def get_farthest_point(start_lat, start_lon, coordinates,source="bosch"):
    if not coordinates or start_lat is None or start_lon is None:
        return None, None  # Return None if data is missing

    max_distance = 0
    Farthest_Latitude, Farthest_Longitude = None, None

    if(source == "bosch"):
        for coord_list in coordinates:
            for lat_lon in coord_list:
                if lat_lon and lat_lon[0] is not None and lat_lon[1] is not None:
                    distance = geodesic((start_lat, start_lon), (lat_lon[0], lat_lon[1])).kilometers
                    if distance > max_distance:
                        max_distance = distance
                        Farthest_Latitude, Farthest_Longitude = lat_lon
    elif(source == "garmin"):
        for lat_lon in coordinates:
            if lat_lon and lat_lon['lat'] is not None and lat_lon['lon'] is not None:
                distance = geodesic((start_lat, start_lon), (lat_lon['lat'], lat_lon['lon'])).kilometers
                if distance > max_distance:
                    max_distance = distance
                    Farthest_Latitude, Farthest_Longitude = lat_lon['lat'], lat_lon['lon']

    return Farthest_Latitude, Farthest_Longitude  # Return the farthest coordinates

def assist_level_percentage(localdata,level):
    for i in range(len(localdata["significant_assistance_level_percentages"])):
        if localdata["significant_assistance_level_percentages"][i]['level'] == level:
            return  localdata["significant_assistance_level_percentages"][i]['value']
    return 0






##########################################################################
# authenticate_with_garmin(tokenstore) :  #Authentifie l'utilisateur avec Garmin Connect et gère le jeton d'authentification.
def authenticate_with_garmin(tokenstore):

    try:
        client = Garmin()
        client.login(tokenstore)
        logmessage("Successfully authenticated to Garmin Connect with current token",logfile,2,False)
        return client

    except (FileNotFoundError, GarminConnectAuthenticationError):
        if(interactive_mode):
            logmessage("Token is not valid or has expired, a new authentication is required",logfile,3,False)
            email = input(fgc.BOLD+"   Enter Garmin Connect email address : "+fgc.ENDC)
            password = getpass(fgc.BOLD+"   Enter Garmin Connect password : "+fgc.ENDC)

            try:
                # Authentification avec les identifiants
                client = Garmin(email, password)
                client.login()  # Connexion

                client.garth.dump(tokenstore)
                logmessage("Authentication was successful, new token is stored in "+tokenstore,logfile,2,False)
                return client
            
            except GarminConnectAuthenticationError:
                logmessage("Authentication denied, check your credentials",logfile,4,USE_SYSLOG)
            except GarminConnectConnectionError as oops:
                logmessage("Could not connect to Garmin Connect : "+format(oops),logfile,4,USE_SYSLOG)
            except GarminConnectTooManyRequestsError:
                logmessage("Too many requests to Garmin Connect, try again later",logfile,4,USE_SYSLOG)
            except Exception as oops:
                logmessage("An un expected error occured : "+format(oops),logfile,4,False)
        else:
            logmessage("Token is not valid or has expired. Run the script in interactive mode with --interactive/-i to enter login/password and regenerate the token.",logfile,4,USE_SYSLOG)
    return None

##########################################################################
# authenticate_with_bosch(cookiestore) :  #Authentifie l'utilisateur avec Bosch eBike Connect et gère d'authentification.
def authenticate_with_bosch(cookiestore):

    try:
        # Reads the cookie
        with open(cookiestore,"r") as file:
            cookiedata = file.read()
        
        # Tests the cookie
        url = "https://www.ebike-connect.com/ebikeconnect/api/portal/activities/trip/headers?max=1&offset="+str(int(round((datetime.strptime(datetime.today().strftime("%Y-%m-%d")+ "-23:59:59.999999", "%Y-%m-%d-%H:%M:%S.%f").timestamp()) * 1000)))
        headers = {
            "Accept": "application/vnd.ebike-connect.com.v4+json, application/json",
            "Protect-from": "CSRF",
            "Cookie": "REMEMBER=" + cookiedata
        }
        response = requests.get(url=url, headers=headers)
        response.raise_for_status()

        # Cookie is valid, we return its data
        logmessage("Successfully authenticated to Bosch eBike Connect with current cookie",logfile,2,False)
        return cookiedata

    except ConnectionError as oops:
        logmessage("Could not connect to Bosch eBike Connect : "+format(oops),logfile,4,USE_SYSLOG)
        return
    
    except (FileNotFoundError,requests.exceptions.HTTPError):
        if(interactive_mode):
            logmessage("Cookie is not present or has expired, a new authentication is required",logfile,3,False)

            # Authenticates with login/password
            email = input(fgc.BOLD+"   Enter Bosch eBike Connect email address : "+fgc.ENDC)
            password = getpass(fgc.BOLD+"   Enter Bosch eBike Connect password : "+fgc.ENDC)
            login_url = "https://www.ebike-connect.com/ebikeconnect/api/portal/login/public"
            
            try:
                response = requests.post(login_url,json={"username": email, "password": password, "rememberme": True})
                response.raise_for_status()
                cookiedata = response.cookies.get("REMEMBER")

                # Stores the cookie in cookiestore
                with open(cookiestore,"w") as file:
                    file.write(cookiedata)
                logmessage("Authentication was successful, new cookie is stored in "+cookiestore,logfile,2,False)
                return cookiedata

            except requests.exceptions.HTTPError as oops:
                logmessage("Could not login to Bosch eBike Connect : "+format(oops),logfile,4,USE_SYSLOG)
                return
            except Exception as oops:
                logmessage("An unexpected error occured : "+format(oops),logfile,4,USE_SYSLOG)
                return
        else:
            logmessage("Cookie is not valid or has expired. Run the script in interactive mode with --interactive/-i to enter login/password and regenerate the cookie.",logfile,4,USE_SYSLOG)
    return None

##########################################################################
# get_garmin_connect_activities(garmin_client,max_activity_age) : 
def get_garmin_connect_activities(garmin_client,max_activity_age):

    # Defines the date bounds of activities - dates are expected in ISO format
    start_date = (datetime.now() - timedelta(days=max_activity_age)).strftime("%Y-%m-%d")
    end_date = (datetime.now()).strftime("%Y-%m-%d")

    # Retreives activities through the client
    logmessage(f"  Retreiving activities between {start_date} and {end_date}",logfile,1,False)
    try:
        activities = garmin_client.get_activities_by_date(start_date, end_date)
        # Filter for cycling activities 
        cycling_activities = [
            activity for activity in activities if "cycling" in activity.get("activityType", {}).get("typeKey")
        ]
        activities_count = len(cycling_activities)
        activity_word = "activity" if activities_count < 2  else "activities"

        logmessage(f"  Retrieved {activities_count} {activity_word}",logfile,1,False)

    except Exception as e:
        logmessage(f"  An error occured while retreiving activities : {str(e)}",logfile,4,USE_SYSLOG)

    # Initiates return dataframe
    garmin_activities = pd.DataFrame(columns=TARGET_CSV_HEADERS)

    # Processes each activity
    for activity in cycling_activities:
        
        #Checks that it's not in database
        Activity_ID = int(activity['activityId'])
        existing_activity = processed_db[processed_db['Activity_ID'] == Activity_ID]
        if not existing_activity.empty:
            processed_time = existing_activity['Processed_Time'].values[0]
            logmessage(f"  Activity with ID '{Activity_ID}' was already processed at {processed_time}",logfile,3,USE_SYSLOG)
            continue

        logmessage(f"  Activity : {Activity_ID} '{activity['activityName']}' started on {activity['startTimeLocal']}",logfile,1,False)
        
        # Retrieves Activity detail
        logmessage(f"  Retrieving activity informations & details",logfile,1,False)
        activity_infos = garmin_client.get_activity(Activity_ID)
        activity_details = garmin_client.get_activity_details(Activity_ID)

        # Processes the available fields

        # GUID
        Activity_GUID =  activity_infos['activityUUID']['uuid'].upper()  #str(uuid.uuid4()).upper()

        # Title
        Activity_Title = activity['activityName']

        #Activity 
        Activity_Type = DEFAULT_SPORT

        # Description
        Activity_Description = Activity_Title

        # Link
        Activity_Link = GARMIN_LINKROOT+str(Activity_ID)

        # Bike
        Bike = DEFAULT_GARMIN_BIKE

        # Start Time as ISO Date/Time
        Start_Time = activity['startTimeLocal']

        # End Time is Start Time + Total Duration
        end = datetime.strptime(Start_Time,"%Y-%m-%d %H:%M:%S") + timedelta(seconds=activity['elapsedDuration'])
        End_Time = end.strftime("%Y-%m-%d %H:%M:%S")

        # Start and End Coordinates
        Start_Latitude = activity['startLatitude']
        Start_Longitude = activity['startLongitude']
        End_Latitude = activity['endLatitude']
        End_Longitude = activity['endLongitude'] 


        # Find the farthest point from Start_Latitude, Start_Longitude
        gps_data = activity_details.get("geoPolylineDTO", {}).get("polyline", [])
        if Start_Latitude is not None and Start_Longitude is not None:
            Farthest_Latitude, Farthest_Longitude = get_farthest_point(
                Start_Latitude, Start_Longitude, gps_data,"garmin"
          )
            
        # Start, Farthest, End Locations -- uses Geopy - AI generated
        if Start_Latitude is not None and Start_Longitude is not None:
            Location = get_city_name(Start_Latitude, Start_Longitude)
            Location_farthest = get_city_name(Farthest_Latitude, Farthest_Longitude)
            Location_end = get_city_name(End_Latitude, End_Longitude)
            Trip_Locations = f"{Location} - {Location_farthest} - {Location_end}"

        # Durations
        Duration_Total = activity['elapsedDuration']
        Duration_Moving = activity['movingDuration']
        Duration_Activity = activity['duration']
        
        # Distance
        Distance = activity['distance']

        # Calories
        Calories = activity['calories']

        # Speeds
        Speed_Average = activity['averageSpeed']*3.6
        Speed_Max = activity['maxSpeed']*3.6

        # Heart Rate
        try:
            Heart_Rate_Average = activity['averageHR']
            Heart_Rate_Max = activity['maxHR']
        except KeyError:
            Heart_Rate_Average = None
            Heart_Rate_Max = None

        # Cadence
        try:
            Cadence_Average = activity['averageBikingCadenceInRevPerMinute']
            Cadence_Max = activity['maxBikingCadenceInRevPerMinute']
        except KeyError:
            Cadence_Average = None
            Cadence_Max = None
        
        # Altitude
        Altitude_Min = activity['minElevation']
        Altitude_Max =  activity['maxElevation']
        Elevation_Gain =  activity['elevationGain']
        Elevation_Loss =  activity['elevationLoss']

        # Device Information
        Device_Serial_Number = activity['deviceId']
        Device_Part_Number = None
        Engine_Serial_Number = None
        Engine_Part_Number = None

        # Power
        try:
            Power_Average = activity['powerAverage']
            Power_Max = activity['powerMax']
        except KeyError: 
            Power_Average = None
            Power_Max = None

        # Assist
        Power_Cyclist_Pct = None
        Power_Engine_Pct = None
        Assist_0_Pct = None
        Assist_1_Pct = None
        Assist_2_Pct = None
        Assist_3_Pct = None
        Assist_4_Pct = None

        # Temperatures
        Temperature_Min = activity['minTemperature']
        Temperature_Max = activity['maxTemperature']
        Temperature_Average = activity_infos['summaryDTO']['averageTemperature']

        # Zones
        Zone1Start = None
        Zone2Start = None
        Zone3Start = None
        Zone3End = None

        # ADD HERE SOME PERSONNAL CALCULATIONS >>>

            # If there is no cadence information, then it's bike "bike2"
        if Cadence_Average is None:
            Bike = "bike2"

            # If 'bike2' was used and distance is less than 10km, then it's likely to be a "Utilitary" activity
        if Bike == "bike2" and Distance < 10000:
            Activity_Type = UTILITARY_SPORT
            
        # <<---

        # Ajout de la ligne au DataFrame
        new_line = {col: eval(col) for col in garmin_activities.columns}
        garmin_activities = pd.concat([garmin_activities, pd.DataFrame([new_line])], ignore_index=True)

    #Renvoie le DataFrame
    return garmin_activities

##########################################################################
# get_bosch_ebike_connect_activities(bosch_cookie,max_trips) : 
def get_bosch_ebike_connect_activities(bosch_cookie,max_trips,force) : 

    # Defines bounds of trips - API expects a milisecond timestamp
    if force :
        logmessage("  The --bosch-force switch was provided, trips until now will be retreived",logfile,3,USE_SYSLOG)
        end_date = int(datetime.now().timestamp()*1000)
    else:
        end_date = int(datetime.now().replace(hour=0, minute=0, second=0, microsecond=0).timestamp()*1000)
    
    # Lists available trips
    end_date_formated = datetime.strftime(datetime.fromtimestamp(int(end_date/1000)),"%d/%m/%Y %H:%M:%S")
    logmessage(f"  Retreiving {max_trips} trip(s) until {end_date_formated}",logfile,1,False)

    try:
        url = (f"https://www.ebike-connect.com/ebikeconnect/api/portal/activities/trip/headers?max={max_trips}&offset={end_date}")
        headers = {
        "Accept": "application/vnd.ebike-connect.com.v4+json, application/json",
        "Protect-from": "CSRF",
        "Cookie": "REMEMBER=" + bosch_cookie
        }
        logmessage(url,logfile)
        response = requests.get(url=url, headers=headers)
        response.raise_for_status()
    except requests.exceptions.HTTPError as oops :
        logmessage("  Could not retreive list of trips from Bosch eBike Connect : "+format(oops),logfile,4,USE_SYSLOG)
        return
    
    available_trips = []
    for trip in json.loads(response.content):
        available_trips.append(trip['id'])
    
    trips_count = len(available_trips)
    trip_word = "trip" if trips_count < 2  else "trips"

    logmessage(f"  {trips_count} {trip_word} available in API",logfile,1,False)
    logmessage(f"{available_trips}",logfile)

    # Initiates return dataframe
    bosch_trips = pd.DataFrame(columns=TARGET_CSV_HEADERS)

    # Processes each trip
    for trip in available_trips:

        #Checks that it's not in database
        Activity_ID = int(trip)

        existing_activity = processed_db[processed_db['Activity_ID'] == Activity_ID]
        if not existing_activity.empty:
            processed_time = existing_activity['Processed_Time'].values[0]
            logmessage(f"  Trip with ID '{Activity_ID}' was already processed at {processed_time}",logfile,3,USE_SYSLOG)
            continue

        # Retrieves the trip information
        logmessage(f"  Retrieving information for trip with id ': '{Activity_ID}'",logfile,1,False)
        url = f"https://www.ebike-connect.com/ebikeconnect/api/activities/trip/details/{Activity_ID}"
        headers = {
        "Accept": "application/vnd.ebike-connect.com.v4+json, application/json",
        "Protect-from": "CSRF",
        "Cookie": "REMEMBER=" + bosch_cookie
        }
        try:
            logmessage(url,logfile)
            response = requests.get(url=url, headers=headers)
            response.raise_for_status()
            logmessage(f"Downloaded : {len(response.content)} bytes",logfile)
        except requests.exceptions.HTTPError as oops :
            logmessage(f"  Could not retreive informations from trip {Activity_ID} : "+format(oops),logfile,4,USE_SYSLOG)
            continue
        
        # Analyses trip information
        trip_data = json.loads(response.content)
        
        logmessage(f"  Activity : {Activity_ID} started on {datetime.fromtimestamp(int(trip_data['start_time']) / 1000).strftime("%d/%m/%Y %H:%M")}",logfile,1,False)

        # GUID
        Activity_GUID = str(uuid.uuid4()).upper()

        # Start and End Coordinates - AI Generated
            # Retrieve the list of coordinates
        coordinates = trip_data.get("coordinates", [])
        Start_Latitude, Start_Longitude = None, None
        End_Latitude, End_Longitude = None, None

            # Find the first non-null latitude/longitude
        for coord_list in coordinates:
            for lat_lon in coord_list:
                if lat_lon and lat_lon[0] is not None and lat_lon[1] is not None:
                    Start_Latitude, Start_Longitude = lat_lon
                    break  # Stop as soon as we find a value
            if Start_Latitude is not None:
                break  # Stop the main loop as well

            # Find the last non-null latitude/longitude (by iterating in reverse)
        for coord_list in reversed(coordinates):
            for lat_lon in reversed(coord_list):
                if lat_lon and lat_lon[0] is not None and lat_lon[1] is not None:
                    End_Latitude, End_Longitude = lat_lon
                    break
            if End_Latitude is not None:
                break

        # Find the farthest point from Start_Latitude, Start_Longitude - 
        if Start_Latitude is not None and Start_Longitude is not None:
            Farthest_Latitude, Farthest_Longitude = get_farthest_point(
                Start_Latitude, Start_Longitude, trip_data.get("coordinates", [])
          )

        # Start, Farthest, End Locations -- uses Geopy - AI generated
        if Start_Latitude is not None and Start_Longitude is not None:
            Location = get_city_name(Start_Latitude, Start_Longitude)
            Location_farthest = get_city_name(Farthest_Latitude, Farthest_Longitude)
            Location_end = get_city_name(End_Latitude, End_Longitude)
            Trip_Locations = f"{Location} - {Location_farthest} - {Location_end}"

        # Title
        if(not Location_farthest == Location):
            destinations = f"{Location}-{Location_farthest}-{Location_end}"
        else:
            destinations = Location
        Activity_Title = destinations
        logmessage(f"  Activity title : {Activity_Title}",logfile,1,False)

        # Link
        Activity_Link = BOSCH_LINKROOT+str(Activity_ID)

        # Bike
        Bike = DEFAULT_BOSCH_BIKE

        # Start Time/End Time as ISO Date/Time
        Start_Time = datetime.fromtimestamp(int(trip_data['start_time']) / 1000).strftime("%Y-%m-%d %H:%M:%S")
        End_Time = datetime.fromtimestamp(int(trip_data['end_time']) / 1000).strftime("%Y-%m-%d %H:%M:%S")

        # Durations
        Duration_Total = round((int(trip_data['end_time'])-int(trip_data['start_time']))/1000)
        Duration_Moving = round(int(trip_data['driving_time'])/1000)
        Duration_Activity = round(int(trip_data['operation_time'])/1000)

        # Distance
        Distance = trip_data['total_distance'] if trip_data['total_distance'] > 0 else None

        # Calories
        Calories = trip_data['calories'] if trip_data['calories'] > 0 else None

        # Speeds
        Speed_Average = trip_data['avg_speed']
        Speed_Max = trip_data['max_speed']

        # Heart Rate
        Heart_Rate_Average = trip_data['avg_heart_rate'] if trip_data['avg_heart_rate'] > 0 else None
        Heart_Rate_Max = trip_data['max_heart_rate'] if trip_data['max_heart_rate'] > 0 else None

        # Cadence
        Cadence_Average = trip_data['avg_cadence']
        Cadence_Max = trip_data['max_cadence']

        # Altitude
        altitudes = trip_data.get("portal_altitudes", [])
        altitudes_filtered = [
            alt for sublist in altitudes for alt in sublist 
            if isinstance(alt, (int, float)) and VALID_ALTITUDE_RANGE[0] <= alt <= VALID_ALTITUDE_RANGE[1]
        ]
        Altitude_Min = min(altitudes_filtered) if altitudes_filtered else None
        Altitude_Max =  max(altitudes_filtered) if altitudes_filtered else None
        Elevation_Gain =  trip_data['elevation_gain']
        Elevation_Loss =  trip_data['elevation_loss']

        # Power
        power_values = trip_data.get("power_output", [])
            # Flatten the nested list and filter out invalid values
        valid_power_values = [
            power for sublist in power_values for power in sublist
            if isinstance(power, (int, float)) and power > 0  # Ensure only positive numerical values
        ]

        Power_Average = trip_data['average_driver_power']
        Power_Max = max(valid_power_values) if valid_power_values else None


        # Assist
        Power_Cyclist_Pct = trip_data['total_driver_consumption_percentage']
        Power_Engine_Pct = trip_data['total_battery_consumption_percentage']

        Assist_0_Pct = (round(assist_level_percentage(trip_data,0),2))
        Assist_1_Pct = (round(assist_level_percentage(trip_data,1),2))
        Assist_2_Pct = (round(assist_level_percentage(trip_data,2),2))
        Assist_3_Pct = (round(assist_level_percentage(trip_data,3),2))
        Assist_4_Pct = (round(assist_level_percentage(trip_data,4),2))

        # Temperatures
        Temperature_Min = None
        Temperature_Max = None

        # Zones
        Zone1Start = None
        Zone2Start = None
        Zone3Start = None
        Zone3End = None

        # Device Information
        Device_Serial_Number = trip_data['bui_decoded_serial_number']
        Device_Part_Number = trip_data['bui_decoded_part_number']
        Engine_Serial_Number = trip_data['drive_unit_decoded_serial_number']
        Engine_Part_Number = trip_data['drive_unit_decoded_part_number']

        #Activity 
        Activity_Type = DEFAULT_SPORT

        # Description
        Activity_Description = Activity_Title

        # ADD HERE SOME PERSONNAL CALCULATIONS >>>

            # If Farthest = Start > Utilitary
        if Location == Location_farthest:
            Activity_Type = UTILITARY_SPORT

            # If Farthest = "BIKE2WORK_LOCATION"
        if Location_farthest == BIKE2WORK_LOCATION:
            Activity_Type = BIKE2WORK_SPORT
            Activity_Title = f"Velotaf"

        # Ajout de la ligne au DataFrame
        new_line = {col: eval(col) for col in bosch_trips.columns}
        bosch_trips = pd.concat([bosch_trips, pd.DataFrame([new_line])], ignore_index=True)

    #Renvoie le DataFrame
    return bosch_trips       

#####################################################################################################################################

def main():

##############################################
# EXECUTION HEADER
    # Tempoary sets the debug flag to Default (until arguments are parsed )
    global debug_mode
    debug_mode = DEFAULT_IS_DEBUG

    global logfile
    logfile=logmessage("Let's go!",None,1)
    logmessage("#**************************************************************************************#",logfile,1)
    logmessage("# biketripsync.py                                                                      #",logfile,1)
    logmessage("# Exports new Garmin Connect activities and Bosch eBike Connect trip to CSV            #",logfile,1)
    logmessage("# and uploads it to OneDrive for Business                                               #",logfile,1)
    logmessage("#                                                             <!            @          #",logfile,1)
    logmessage("#                                                              !___   _____`<,!_!      #",logfile,1)
    logmessage("# by @CyclisteUrbain                                           !(*)!--(*)---/(*)       #",logfile,1)
    logmessage("########################################################################################",logfile,1)


    # Configuration d'argparse pour les arguments de ligne de commande
    parser = argparse.ArgumentParser(description="Exports new Garmin Connect activities and Bosch eBike Connect trip to CSV.")
    parser.add_argument(
        "--garmin","-g",
        action='store_true',
        default=False,
        help="Get activities from Garmin Connect"
    )
    parser.add_argument(
        "--bosch","-b",
        action='store_true',
        default=False,
        help="Get trips from Bosch eBike Connect"
    )
    parser.add_argument(
        "--days","-d",
        type=int,
        default=GARMIN_MAX_AGE_DAYS_DEFAULT,
        help="Number of days in the past to look for new activities from Garmin"
    )
    parser.add_argument(
        "--trip-count","-t",
        type=int,
        default=BOSCH_MAX_NUMBER_OF_TRIPS,
        help="Number of trips to load from Bosch eBike Connect"
    )
    parser.add_argument(
        "--resetdb","-r",
        action='store_true',
        default=False,
        help="Reset the file that contains already dowloaded activities and trips"
    )
    parser.add_argument(
        "--bosch-force","-bf",
        action='store_true',
        default=False,
        help="Forces retrieval of Bosch ebike connect trips from the same day (defaut excludes them as the trip might not be finished)"
    )
    parser.add_argument(
        "--upload","-u",
        action='store_true',
        default=False,
        help="Upload files to OneDrive/SharePoint"
    )
    parser.add_argument(
            "--debug","-dbg",
            action='store_true',
            default=DEFAULT_IS_DEBUG,
            help="Activates debug mode"
        )
    parser.add_argument(
            "--interactive","-i",
            action='store_true',
            default=DEFAULT_IS_INTERACTIVE,
            help="Runs in interactive mode that prompts for login/password if needed"
        )
    args = parser.parse_args()

    ### CHECKS ARGUMENTS

    # -bosch and -garmin : at least one should be provided
    if not (args.bosch or args.garmin):
        logmessage("At least one of the two arguments --bosch/-b or --garmin/-g should be provided",logfile,5,USE_SYSLOG)
        return
    else:
        retreive_bosch = False
        retrieve_garmin = False
        if args.bosch:
            retreive_bosch = True
            logmessage("Bosch eBike Connect trips will be retreived",logfile,1,USE_SYSLOG)
        if args.garmin:
            retrieve_garmin = True
            logmessage("Garmin activities will be retreived",logfile,1,USE_SYSLOG)
    
    # --days should be less than LOCAL_DB_MAX_DAYS, and over 1 unless only --garmin was set or --bosch-force was set
    if args.days > LOCAL_DB_MAX_DAYS :
        logmessage("--days should be under the LOCAL_DB_MAX_DAYS setting ("+str(LOCAL_DB_MAX_DAYS)+")",logfile,5,USE_SYSLOG)
        return
    if args.days < 2 and retreive_bosch and not(retrieve_garmin) and not(args.bosch_force):
        logmessage("--days should be over 1 unless only --garmin was set or --bosch-force was set",logfile,5,USE_SYSLOG)
        return
    max_activity_age = args.days
    logmessage("Maximum activity/trip age is set to "+str(max_activity_age)+" days",logfile,1,USE_SYSLOG)

    # --trip-count
    max_trip_count = args.trip_count

    # --bosch-force
    bosch_force = args.bosch_force

    # --upload
    upload_to_onedrive = args.upload

    # --debug
    debug_mode = args.debug

    # --interactive
    global interactive_mode
    interactive_mode = args.interactive

    ### LOAD OR RESETS PROCESSED DB
    global processed_db
    # Resets the DB if that switch was provided
    if args.resetdb:
        logmessage("Resetting processed activities/trip db at "+LOCAL_DB_CSV,logfile,3,USE_SYSLOG)
        try:
            with open(LOCAL_DB_CSV, "w", encoding="utf-8") as file:
                file.write("Activity_ID,Processed_Time\n")
                file.close()
            logmessage(f"Database in file '{LOCAL_DB_CSV}' was successfully reset",logfile,2,False)
        except PermissionError:
            logmessage(f"Permission denied on '{LOCAL_DB_CSV}'. db could not be reset. Please be aware that this might cause other issues",logfile,4,USE_SYSLOG)
    
    # Tries to load the database file, and creates it if it doesn't exist
    logmessage("Loading processed activities/trip db from "+LOCAL_DB_CSV,logfile,1,USE_SYSLOG)
    try:
        processed_db = pd.read_csv(LOCAL_DB_CSV,sep=",",)
        logmessage("Loaded "+str(processed_db.shape[0])+" entries from database file",logfile,1,False)
    except FileNotFoundError:
        logmessage("Database file does not exist, creating it at "+LOCAL_DB_CSV,logfile,1,USE_SYSLOG)
        with open(LOCAL_DB_CSV, "w", encoding="utf-8") as file:
            file.write("id;start_date\n")
            file.close()
        processed_db = pd.read_csv(LOCAL_DB_CSV,sep=";",)


    ### INITIATE TARGET STRUCTURE
    try:
        activities = pd.DataFrame(columns=TARGET_CSV_HEADERS)
        logmessage("Successfully initiated target activities structure",logfile,2,False)
    except Exception as oops:
        logmessage("An error occured while initating target activities structure, check TARGET_CSV_HEADERS configuration item",logfile,5,USE_SYSLOG)
        return

    ### AUTHENTICATES WITH GARMIN
    if(retrieve_garmin):
        logmessage("Authenticating to Garmin Connect",logfile,1,False)
        garmin_client = authenticate_with_garmin(GARMIN_TOKEN_PATH)
        if not garmin_client:
            logmessage("Something went wrong with Garmin Connect authentication, GC activities will be ignored",logfile,4,USE_SYSLOG)
            retrieve_garmin = False

    ### AUTHENTICATES WITH BOSCH
    if(retreive_bosch):
        logmessage("Authenticating to Bosch eBike Connect",logfile,1,False)
        bosch_cookie = authenticate_with_bosch(BOSCH_COOKIE_PATH)
        if not bosch_cookie:
            logmessage("Something went wrong with Bosch eBike Connect authentication, BeBC trips will be ignored",logfile,4,USE_SYSLOG)
            retreive_bosch = False

    ### RETRIEVES GARMIN CONNECT ACTIVITIES
    if(retrieve_garmin):
        logmessage("Retreiving Garmin Connect Activities",logfile,1,False)
        garmin_connect_activities = get_garmin_connect_activities(garmin_client,max_activity_age)
        activities = pd.concat([activities, garmin_connect_activities], ignore_index=True)

    ### RETRIEVES BOSCH EBIKE CONNECT TRIPS 
    if(retreive_bosch):
        logmessage("Retreiving Bosch eBike Connect trips",logfile,1,False)
        logmessage(f"Cookie Data = {bosch_cookie}",logfile)
        bosch_trips = get_bosch_ebike_connect_activities(bosch_cookie,max_trip_count,bosch_force)
        activities = pd.concat([activities, bosch_trips], ignore_index=True)

    ### WRITES CSV FILES
    files_to_upload = []
    if activities.shape[0] > 0:
        logmessage("Saving activities to CSV files",logfile,1,False)
        saved_activities = 0
        # Loop through each row in activities
        for _, row in activities.iterrows():
            try:
                # Extract Start_Date and format it for filename
                start_date = row["Start_Time"].replace(":", "-").replace(" ", "_")  # Remove invalid characters if needed
                filename = f"{start_date}.csv"
                
                # Save the single-row DataFrame to CSV
                row.to_frame().T.to_csv(f"{LOCAL_CSV_PATH}{filename}", index=False, encoding="utf-8",sep=";")

                # Add entry to processed_db
                new_entry = pd.DataFrame({
                    "Activity_ID": [row["Activity_ID"]],
                    "Processed_Time": [datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
                })

                # Append the new entry to processed_db
                processed_db = pd.concat([processed_db, new_entry], ignore_index=True)
                
                saved_activities += 1
                files_to_upload.append(filename) # Builds a list of files to be uploaded
                logmessage(f"Saved activity {row['Activity_ID']} '{row['Activity_Title']}' to {filename}",logfile,2,False)

            except Exception as oops:
                logmessage(f"Could not save activity '{row["Activity_Title"]}' from {start_date} : "+format(oops),logfile,4,USE_SYSLOG)
        activity_word = "activities" if saved_activities > 1 else "activity"
        logmessage(f"Saved {saved_activities} {activity_word}/{len(activities)} to files",logfile,2,USE_SYSLOG)
    else:
        logmessage(f"No activity to be saved",logfile,1,USE_SYSLOG)

    ### UPLOAD CSV FILES
    files_to_upload_count = len(files_to_upload)
    if(files_to_upload_count and upload_to_onedrive):

        file_word = "files" if files_to_upload_count > 1 else "file"
        logmessage(f"{files_to_upload_count} {file_word} to be uploaded to OneDrive for Business",logfile,1,False)

        # Initiates authentification
        logmessage("Authenticating to OneDrive",logfile,1,False)
        app = msal.ConfidentialClientApplication(ONEDRIVE_CLIENT_ID, authority=ONEDRIVE_AUTHORITY, client_credential=ONEDRIVE_CLIENT_SECRET)
        token_response = app.acquire_token_for_client(scopes=ONEDRIVE_SCOPES)
    
        if "access_token" in token_response:
            ACCESS_TOKEN = token_response["access_token"]
            auth_succeess = True
        else:
            logmessage(f"Authentication to OneDrive Failed with {token_response['error']} error: {token_response['error_description']}",logfile,4,USE_SYSLOG)
            auth_succeess = False

        # Upload files if auth was successful
        if auth_succeess:
            uploaded_files = 0
            for file in files_to_upload:
                local_file_path = f"{LOCAL_CSV_PATH}{file}"
                file_name = file
                upload_url = f"https://graph.microsoft.com/v1.0/users/{ONEDRIVE_USER_ID}/drive/{ONEDRIVE_PATH}{file_name}:/content"

                with open(local_file_path, "rb") as f:
                    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}", "Content-Type": "application/octet-stream"}
                    response = requests.put(upload_url, headers=headers, data=f)

                if response.status_code in [200, 201]:
                    logmessage(f"Uploaded file {file}",logfile,2,USE_SYSLOG)
                    uploaded_files += 1
                else:
                    logmessage(f"Upload of file {file} failed : {response.text}",logfile,4,USE_SYSLOG)
            
            message_level = 2 if uploaded_files == files_to_upload_count else 3
            file_word = "files" if uploaded_files > 1 else "file"

            logmessage(f"Uploaded {uploaded_files} {file_word} of {files_to_upload_count}",logfile,message_level,USE_SYSLOG)

    ### UPDATES DATABASE
    # Filter the DataFrame
    logmessage("Updating processed activities database",logfile,1,False)
    processed_db["Processed_Time"] = pd.to_datetime(processed_db["Processed_Time"])
    threshold_date = datetime.now() - timedelta(days=LOCAL_DB_MAX_DAYS)
    processed_db = processed_db[processed_db["Processed_Time"] > threshold_date]
    processed_db.to_csv(LOCAL_DB_CSV, index=False, encoding="utf-8")

######## CALL TO MAIN
if __name__ == "__main__":
    main()