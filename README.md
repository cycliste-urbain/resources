# Resources for curious cyclists

This repository aims at sharing a few scripts and tools I coded to help with the devices I use cycling around. Feel free to fork and adapt to your own use!

## VeloBots
The **Velobots** is a set of Python scripts that retreives bike trafic data made available by Paris and Montreuil through their OpenData plateforms. They populates a MySQL database with these information and then analyses them to publish statistic via Mastodon posts.

➡️ [Velobots](/VeloBots)

## BikeTripSync
**biketripsync.py** is a script that downloads bike ride informations from Garmin Connect and Bosch eBike Connect and create CSV files with a common structure. It aims at providing curious cyclist with a way to create their own database of bike ride history, so they can chose what statistics they want to create. 

➡️ [BikeTripSync](/BikeTripSync)


## eBCDownload.py

Through its connected consoles (Kiox, Nyon), the Bosch eBike system collects a huge amount of data, ranging from speed to location, even power usage. The eBike Connect portal shows them in a nice way, but the CSV export feature is limited.

**eBCDownload.py** is a Python 3.x script that downloads all collected data into .json files containing :

- Global information (start time, end time, riding time, distance, max & avg speed, max & avg cadence, average cyclist power, elevation...)
- Cadence information by the second
- Heart rate by the second
- Speed by the second
- GPS location by the second
- Altitude by the second
- Cyclist power by the second
- Cyclist/Battery power ratio
- Assists usage mode ratio
- Bike computer information (serial, type...)

## eBC2DC.py

Sigma Sports was one of the first bike computer manufacturer to offer the ability to save bike ride data to an application and in the cloud. Their _Data Center_ application allows users of their devices to track rides efficiently, even from their non GPS bike computers. But it hasn't evolved a lot and it lacks external connections to be one's main tracking hub like Strava or Garmin Connect, which is a shame as it's the only application that allows to track non GPS, simple bike computers.

The **eBC2DC.py** script translates CSV file exports from the Bosch eBike Connect portal into SMF files that can be then imported into Sigma Data Center, allowing Bosch eBike users that have other bikes set up with Sigma bike computers to track all their ride information into a single tool.

## eBCSMFExporter.py

A bit like _eBC2DC.py_ **eBCSMFExporter.py** aims to create a SMF file that can be then exported into Sigma's Data Center application. But where eBC2DC requires a CSV file export, eBCSMFExporter downloads the necessary information from the Bosch eBike Connect API

## GCSMFExporter

**GCSMFExporter.py** is a script that uses the _garminconnect_ Python module in order to retreive cycling activites from Garmin Connect. It then creates a SMF file that can be imported into Sigma Data Center.  

## GC2SMF

**GC2SMF.py** is a script that create .smf files for Sigma Data Center from a CSV file downloaded from Garmin Connect web site. Alas, the CSV file structure is not stable as Garmin regularly changes it, and on top of that, it is localized which makes it difficult to reuse.
I keep it for archive, but I strongly recommend using **GCSMFExporter** instead if you're looking for that feature. 

## DerushCX.sh & DerushCX.py

**DerushCX** aims at making easier to derush videos made with a GoPro camera.
It loops through the directory passed as argument, and extract parts of the videos it contains based on the use of the _highlight_ feature (Power/Mode button pressed while filming). By default, it will extract 55s before and 5s after the highlight button on the camera was pressed.

[twitter.com/CyclisteUrbain](https://twitter.com/CyclisteUrbain)

      <!            @
       !___   _____`\<,!_!
       !(*)!--(*)---/(*)

Made in Montreuil with :peach:
