#!/usr/bin/python3

#########################################################################################
# GCSMFExporter.py                                                                      #
# >> Exports Garmin Connect activities to Sigma Data Center SMF files                   #
#  @CyclisteUrbain                                                                      #
#                                                             <!            @           #
#                                                              !___   _____`\<,!_!      #
#                                                              !(*)!--(*)---/(*)        #
#########################################################################################

import os
from datetime import datetime, timedelta
from getpass import getpass
from xml.dom.minidom import Document
from garminconnect import *
import argparse
import uuid

# Default output location
OUTPUT_DIR    = "~/Documents/"
# Allowed sports & default
ALLOWED_SPORTS = ["cycling","bmx","racing_bicycle","mountainbike","triathlon","ebike","indoor_cycling","cyclecross","enduro"]
DEFAULT_SPORT  = ALLOWED_SPORTS[0]
# Default Bike
DEFAULT_BIKENUMBER = 2
# Durée de validité du jeton en jours
TOKEN_VALIDITY_DAYS = 30  
# Emplacement du jeton par défaut
DEFAULT_TOKEN = "~/.garth"
# Emplacement de sortie par défaut
DEFAULT_OUTPATH = "~/Documents"

#Colors
class fgc:
    HEADER  = '\033[95m'
    OKBLUE  = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL    = '\033[91m'
    ENDC    = '\033[0m'
    BOLD    = '\033[1m'

def parse_date(date_str):
    # Convertit une date au format JJ/MM/AAAA en objet datetime.
    try:
        # Convertir la date en objet datetime
        date_obj = datetime.strptime(date_str, "%d/%m/%Y")
        # Retourner la date sous forme de chaîne ISO
        return date_obj.strftime("%Y-%m-%d")
    except ValueError:
        raise ValueError(f"Le format de la date '{date_str}' est invalide. Utilisez JJ/MM/AAAA.")

def authenticate_with_garmin(tokenstore):
    #Authentifie l'utilisateur avec Garmin Connect et gère le jeton d'authentification.

    try:
        client = Garmin()
        client.login(tokenstore)
        print(fgc.OKGREEN+"Authentification par jeton réussie."+fgc.ENDC)
        return client

    except (FileNotFoundError, GarminConnectAuthenticationError):
        print(fgc.WARNING+"Jeton d'authentification non présent ou expiré, une nouvelle authentification est requise"+fgc.ENDC)
        email = input(fgc.BOLD+"   Entrez votre adresse e-mail Garmin Connect : "+fgc.ENDC)
        password = getpass(fgc.BOLD+"   Entrez votre mot de passe Garmin Connect : "+fgc.ENDC)

        try:
            # Authentification avec les identifiants
            client = Garmin(email, password)
            client.login()  # Connexion
            print(fgc.OKGREEN+"Authentification réussie."+fgc.ENDC)

            client.garth.dump(tokenstore)
            print(fgc.OKGREEN+"Jeton sauvegardé dans "+tokenstore+"."+fgc.ENDC)

            return client
        except GarminConnectAuthenticationError:
            print("Erreur : Échec de l'authentification. Vérifiez vos identifiants.")
        except GarminConnectConnectionError:
            print("Erreur : Problème de connexion avec Garmin Connect.")
        except GarminConnectTooManyRequestsError:
            print("Erreur : Trop de requêtes envoyées. Réessayez plus tard.")
        except Exception as e:
            print("Une erreur inattendue s'est produite :", str(e))

    return None

def list_cycling_activities(client, start_date, end_date):
    # Liste les activités de type 'cycling' entre deux dates.

    try:
        print(f"Récupération des activités 'cycling' entre {start_date} et {end_date}...")
        activities = client.get_activities_by_date(start_date, end_date)
        
        # Filtrer uniquement les activités de type "cycling"
        cycling_activities = [
            activity for activity in activities if "cycling" in activity.get("activityType", {}).get("typeKey")
        ]
        return cycling_activities
        
    except Exception as e:
        print(f"Erreur lors de la récupération des activités : {str(e)}")

def export_activity(activity):

    #ID------------------------------------------
    activity_id = str(activity['activityId'])

    #TITLE----------------------------------------
    ride_title = activity['activityName']

    #START TIME-----------------------------------
    #Formated > Formated 	
    start_time = datetime.strptime(activity['startTimeLocal'],"%Y-%m-%d %H:%M:%S")
    ride_start_date = datetime.strftime(start_time,"%a %b %d %H:%M:%S %Y")

    print("\r\n"+fgc.BOLD+activity_id+" : "+ride_title+" le "+datetime.strftime(start_time,"%d %m %Y à %H:%M")+fgc.ENDC)

    #Affiche un avertissement si l'altitude n'a pas été corrigée
    if activity['elevationCorrected'] == False:
        print(fgc.WARNING+"   La correction d'altitude n'est pas activée sur cette activité"+fgc.ENDC)

    #SPORT----------------------------------------
    ride_sport
    print (fgc.BOLD+"   Sport                 : "+fgc.ENDC+ride_sport)

    #BIKE-----------------------------------------
    #Personnel : Si on la la cadence, alors c'est le bike1
    try:
        cad = activity['averageBikingCadenceInRevPerMinute']
        ride_bikenumber = "bike1"
    except KeyError:
        if bikenumber == 0:
            ride_bikenumber = "noBike"
        else:
            ride_bikenumber = "bike"+str(bikenumber)
    print (fgc.BOLD+"   Bike                  : "+fgc.ENDC+ride_bikenumber)

    #DURATION-------------------------------------
    #Seconds > centiseconds
    ride_duration = str(activity['movingDuration']*100)
    print (fgc.BOLD+"   Duration              : "+fgc.ENDC+ride_duration+" centiseconds")

    #DISTANCE-------------------------------------
    #meter > meter
    ride_distance = str(round(activity['distance']))
    print (fgc.BOLD+"   Distance              : "+fgc.ENDC+ride_distance+"m")

    #AVERAGE SPEED--------------------------------
    #m/s > m/s
    avg_speed = float(activity["averageSpeed"])
    ride_avg_speed = str(round(avg_speed,1))
    print (fgc.BOLD+"   Average speed         : "+fgc.ENDC+ride_avg_speed+"m/s")

    #MAX SPEED------------------------------------
    #Km/h > m/s
    max_speed = float(activity["maxSpeed"])
    ride_max_speed = str(round(max_speed,1))
    print (fgc.BOLD+"   Max speed             : "+fgc.ENDC+ride_max_speed+"m/s")

    #AVERAGE CADENCE------------------------------
    #rpm > rpm
    try:
        ride_avg_cadence = str(round(activity['averageBikingCadenceInRevPerMinute']))
        print (fgc.BOLD+"   Average cadence       : "+fgc.ENDC+ride_avg_cadence+"rpm")
    except KeyError:
        print (fgc.BOLD+"   Average cadence       : N/A")


    #MAX CADENCE-------------------------------
    #rpm > rpm
    try:
        ride_max_cadence = str(round(activity['maxBikingCadenceInRevPerMinute']))
        print (fgc.BOLD+"   Max cadence           : "+fgc.ENDC+ride_max_cadence+"rpm")
    except KeyError:
        print (fgc.BOLD+"   Max cadence           : N/A")

    #MAX ALTITUDE------------------------------
    #m > m 
    ride_max_altitude = str(round(1000*activity['maxElevation']))
    print (fgc.BOLD+"   Maximum altitude      : "+fgc.ENDC+ride_max_altitude+"mm")

    #MIN ALTITUDE
    #m > m
    ride_min_altitude = str(round(1000*activity['minElevation']))
    print (fgc.BOLD+"   Minimum altitude      : "+fgc.ENDC+ride_min_altitude+"mm")

    #ELEVATION GAIN-----------------------------
    #m > mm
    ride_elevation_gain = str(round(activity['elevationGain']*1000))
    print (fgc.BOLD+"   Elevation gain        : "+fgc.ENDC+ride_elevation_gain+"mm")

    #ELEVATION LOSS-----------------------------
    #m > mm
    ride_elevation_loss = str(round(activity['elevationLoss']*1000))
    print (fgc.BOLD+"   Elevation loss        : "+fgc.ENDC+ride_elevation_loss+"mm")


    #CALORIES-------------------------------------
    ride_calories = str(round(activity['calories']))
    print (fgc.BOLD+"   Calories              : "+fgc.ENDC+ride_calories+"cal")

    #TEMPERATURES---------------------------------
    ride_max_temperature = str(activity['maxTemperature'])
    ride_min_temperature = str(activity['minTemperature'])
    print (fgc.BOLD+"   Max Temperature       : "+fgc.ENDC+ride_max_temperature+"°c")
    print (fgc.BOLD+"   Min Temperature       : "+fgc.ENDC+ride_min_temperature+"°c")

    #EXTERNAL LINK
    #links to the GarminConnect
    ride_external_link = "https://connect.garmin.com/modern/activity/"+str(activity_id)
    print (fgc.BOLD+"   External link        : "+fgc.ENDC+ride_external_link)

    ##############################################
    # BUILDING XML FILE

    fileDate = datetime.strftime(datetime.now(timezone.utc),"%a %b %d %H:%M:%S %Y")
    guid = str(uuid.uuid4()).upper()
    
    ride_description = """
    Garmin Connect Activity ID : """+str(activity_id)+"""
    """

    try:
        cadence_data = """
                <averageCadence>"""+ride_avg_cadence+"""</averageCadence>
                <maximumCadence>"""+ride_max_cadence+"""</maximumCadence>
        """
    except NameError:
        cadence_data = """
                <!--<averageCadence></averageCadence>-->
                <!--<maximumCadence></maximumCadence>-->
        """

    XMLData = """<?xml version=\"1.0\" encoding=\"utf-8\"?>
    <Activity fileDate=\""""+fileDate+"""\" revision=\"400\">
        <Computer unit="user defined" serial="null" activityType="Cycling" dateCode=""/>
        <GeneralInformation>
            <user color=\"45824\" gender=\"male\">
                <![CDATA[Standard]]>
            </user>
            <sport><![CDATA["""+ride_sport+"""]]></sport>
            <GUID>"""+guid+"""</GUID>
            <bike>"""+ride_bikenumber+"""</bike>
            <name><![CDATA["""+ride_title+"""]]></name>
            <description><![CDATA["""+ride_description+"""]]></description>
            <startDate>"""+ride_start_date+"""</startDate>
            <altitudeDifferencesDownhill>"""+ride_elevation_loss+"""</altitudeDifferencesDownhill>
            <altitudeDifferencesUphill>"""+ride_elevation_gain+"""</altitudeDifferencesUphill>
            """+cadence_data+"""
        <!-- <averageHeartrate>0</averageHeartrate>                -->
        <!-- <maximumHeartrate>0</maximumHeartrate>                -->
        <!-- <averageInclineDownhill>0</averageInclineDownhill>    -->
        <!-- <averageInclineUphill>0</averageInclineUphill>        -->
        <!-- <averageRiseRateUphill>0</averageRiseRateUphill>      -->
        <!-- <averageRiseRateDownhill>0</averageRiseRateDownhill>  -->
            <averageSpeed>"""+ride_avg_speed+"""</averageSpeed>
            <maximumSpeed>"""+ride_max_speed+"""</maximumSpeed>
        <!--    <averageSpeedDownhill>0</averageSpeedDownhill>     -->
        <!--    <averageSpeedUphill>0</averageSpeedUphill>         -->
        <!--    <averagePower>0</averagePower>                     -->
        <!--    <maximumPower>0</maximumPower>                     -->
            <maximumAltitude>"""+ride_max_altitude+"""</maximumAltitude>
            <minimumAltitude>"""+ride_min_altitude+"""</minimumAltitude>
            <calories>"""+ride_calories+"""</calories>
            <distance>"""+ride_distance+"""</distance>
        <!-- <distanceDownhill>0</distanceDownhill> -->
        <!-- <distanceUphill>0</distanceUphill> -->
            <externalLink><![CDATA["""+ride_external_link+"""]]></externalLink>
            <hrMax>0</hrMax>
            <dataType>memory</dataType>
            <linkedRouteId>0</linkedRouteId>
        <!--     <manualTemperature>0</manualTemperature> -->
        <!--     <maximumInclineDownhill>0</maximumInclineDownhill> -->
        <!--     <maximumInclineUphill>0</maximumInclineUphill> -->
        <!--     <maximumRiseRate>0</maximumRiseRate> -->
             <maximumTemperature>"""+ride_max_temperature+"""</maximumTemperature>
        <!--     <minimumRiseRate>0</minimumRiseRate> -->
             <minimumTemperature>"""+ride_min_temperature+"""</minimumTemperature>
        <!--     <pauseTime>0</pauseTime> -->
            <rating>1</rating>
            <feeling>2</feeling>
        <!-- <trainingTimeDownhill>0</trainingTimeDownhill> -->
        <!-- <trainingTimeUphill>0</trainingTimeUphill> -->
            <samplingRate>0</samplingRate>
            <statistic>true</statistic>
            <timeOverZone>0</timeOverZone>
            <timeUnderZone>0</timeUnderZone>
            <trackProfile>0</trackProfile>
            <trainingTime>"""+ride_duration+"""</trainingTime>
            <trainingType/>
            <unitId>0</unitId>
            <weather>0</weather>
            <wheelSize>0</wheelSize>
            <wind>0</wind>
            <zone1Start>0</zone1Start>
            <zone2Start>0</zone2Start>
            <zone3Start>0</zone3Start>
            <zone3End>0</zone3End>
            <activityStatus>none</activityStatus>
            <sharingInfo>{\"sigmaStatisticsId\":\"0\",\"komootId\":\"0\",\"stravaId\":\"0\",\"trainingPeaksId\":\"0\",\"twitterId\":\"0\",\"twoPeaksId\":\"0\",\"facebookId\":\"0\"}</sharingInfo>
            <Participant/>
        </GeneralInformation>
    </Activity>"""

    #Ecriture du XML
    outfilename = "GarminConnect-"+datetime.strftime(start_time,"%Y%m%d-%H%M%S")+".smf"

    with open(out_path+"/"+outfilename, 'w') as outfile:
        outfile.write(XMLData)

    print (fgc.OKGREEN+"\r\nData transfered into file "+out_path+"/"+outfilename+fgc.ENDC)


def main():

##############################################
# EXECUTION HEADER

    print("\r\n"+fgc.HEADER+"#**************************************************************************************#"+fgc.ENDC)
    print(fgc.HEADER+"# gcsmfexporter.py                                                                     #"+fgc.ENDC)
    print(fgc.HEADER+"# Exports Garmin Connect to Sigma Data Center SMF files                                #"+fgc.ENDC)
    print(fgc.HEADER+"#                                                                                      #"+fgc.ENDC)
    print(fgc.HEADER+"#                                                             <!            @          #"+fgc.ENDC)
    print(fgc.HEADER+"#                                                              !___   _____`\<,!_!     #"+fgc.ENDC)
    print(fgc.HEADER+"# by @CyclisteUrbain                                           !(*)!--(*)---/(*)       #"+fgc.ENDC)
    print(fgc.HEADER+"########################################################################################"+fgc.ENDC+"\r\n")


    # Configuration d'argparse pour les arguments de ligne de commande
    parser = argparse.ArgumentParser(description="Script pour accéder aux données Garmin Connect.")
    parser.add_argument(
        "--token-path","-t",
        type=str,
        default=os.path.expanduser(DEFAULT_TOKEN),
        help="Chemin du dossier pour stocker le jeton d'authentification (par défaut : "+DEFAULT_TOKEN+")"
    )
    parser.add_argument(
        "--start-date","-s",
        type=str,
        required=True,
        help="Date de début pour filtrer les activités (format JJ/MM/AAAA)."
    )
    parser.add_argument(
        "--end-date","-e",
        type=str,
        default=datetime.now().strftime("%d/%m/%Y"),
        help="Date de fin pour filtrer les activités (format JJ/MM/AAAA). Par défaut, la date actuelle."
    )
    parser.add_argument(
        "--out-path","-o",
        type=str,
        default=os.path.expanduser(DEFAULT_OUTPATH),
        help="Emplacement par défaut de sortie"
    )
    parser.add_argument(
        '--sport','-sp',
        help='Type de sport',
        type=str,
        default=DEFAULT_SPORT,
        choices=ALLOWED_SPORTS
    )
    parser.add_argument(
        '--bikenumber','-b',
        help='Numéro du vélo (1-3, 0 for autres)',
        type=int,
        default=DEFAULT_BIKENUMBER
    )
    args = parser.parse_args()
    global ride_sport
    ride_sport = args.sport
    global bikenumber
    bikenumber = args.bikenumber
    global out_path
    out_path = args.out_path

    # Convertir les dates du format JJ/MM/AAAA au format ISO AAAA-MM-JJ
    try:
        start_date = parse_date(args.start_date)
        end_date = parse_date(args.end_date)
    except ValueError as e:
        print(e)
        return

    print("Authentification sur Garmin Connect")
    # Authentification et récupération du client
    client = authenticate_with_garmin(args.token_path)
    if not client:
        print(fgc.FAIL+"Impossible de se connecter à Garmin Connect"+fgc.ENDC)
        return

    print("Chargement des activités entre le "+args.start_date+" et le "+args.end_date)
    # Lister les activités 'cycling' entre les dates fournies
    activities = list_cycling_activities(client, start_date, end_date)

    if len(activities) == 0 :
        print(fgc.WARNING+"Aucune activité dans l'ensemble de dates spécifié"+fgc.ENDC)
        return
    else:
        print(str(len(activities))+" activité(s) récupérée(s)")

    #Export des arcivités chargées
    print("Export des activitées")
    for activity in activities : 
        export_activity(activity)






if __name__ == "__main__":
    main()
