#!/usr/bin/python3

#############################################################################
# IMPORTS
import argparse
import os
import pathlib
from datetime import datetime, timedelta
from time import strftime
import subprocess as sp
import json
import shutil


#############################################################################
# GENERAL USE FUNCTIONS AND CLASSES

# COLORS : Used for color formating of output
class fgc:
    HEADER  = '\033[95m'
    OKBLUE  = '\033[94m'
    OKGREEN = '\033[92m'
    OKCYAN  = '\033[96m'
    WARNING = '\033[93m'
    FAIL    = '\033[91m'
    ENDC    = '\033[0m'
    BOLD    = '\033[1m'


##########################################################################
# logmessage(message,logfile,level) : 
#  Prints a message on stdout and writes it to a log file
#    str message : Message to be printed/written (Mandatory)
#    str logfile : Path to the log file, if not specified, a new log file is created, named after the main script and the current timestamp
#    int level   : error level of the message : 0 for Verbose, 1 for Info, 2 for success, 3 for warning, 4 for error, 5 for fatal error. Default is 9 for debug
#  Returns : str with the path to the log file

def logmessage(message:str,logfile:str=None,level:int=9):

    #Create the logfile if not exists
    if(logfile is None):
        logfile = ensure_path(args.log)+os.path.basename(__file__)+"-"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".log"
        try:
            with open(logfile, 'w') as lf:
                lf.write(os.path.basename(__file__)+" : Script startup at "+datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")+"\n")
        except IOError as oups:
            print(fgc.FAIL+"Cannot initiate logfile "+logfile+" : "+format(oups)+fgc.ENDC)

    #Level
    event_time = datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")
    match level:
        case 0:
            event_prefix = event_time+" [VERBOSE] "
            event_color  = fgc.OKBLUE
        case 1:
            event_prefix = event_time+" [INFO]    "
            event_color  = fgc.OKCYAN
        case 2:
            event_prefix = event_time+" [SUCCESS] "
            event_color  = fgc.OKGREEN
        case 3:
            event_prefix = event_time+" [WARNING] "
            event_color  = fgc.WARNING
        case 4:
            event_prefix = event_time+" [ERROR]   "
            event_color  = fgc.FAIL
        case 5:
            event_prefix = event_time+" [FTERROR] "
            event_color  = fgc.FAIL+fgc.BOLD
        case _:
            event_prefix = event_time+" [DEBUG]   "
            event_color  = ""
    
    #Writes to file
    try:
        with open(logfile, 'a') as lf:
            lf.write(event_prefix+message+"\n")
    except IOError as oups:
        print(fgc.FAIL+"Cannot write to logfile "+logfile+" : "+format(oups)+fgc.ENDC)
    
    #Prints to stdout
    print(event_color+event_prefix+message+fgc.ENDC)
    
    #Returns the path to log file
    return logfile


##########################################################################
# ensure_path(path) : 
#  Ensure a string ends with a trailing "/" to be used as a path
#    str path : path (Mandatory)
#  Returns : str 
def ensure_path(path:str):
    if(path.endswith("/")):
        return path
    else:
        return path+"/"


#############################################################################
# SETTINGS
SET_DEFAULT_SOURCE_DIR = "~/Documents/GoPro/Source/"
SET_DEFAULT_OUTPUT_DIR = "~/Documents/GoPro/Output/"
SET_DEFAULT_LOG_DIR = "~/Documents/GoPro/Output/"
SET_DEFAULT_SECONDS_BEFORE = 55
SET_DEFAULT_SECONDS_AFTER = 5
SET_DEFAULT_MIN_DELTA = 5
SET_DEFAULT_FILE_PREFIX = ""
SET_DEFAULT_FILE_SUFFIX = ""
SET_DEFAULT_DB_NAME = ".derushcx.db"
SET_DEFAULT_IGNORE_DB = False
SET_DEFAULT_IGNORE_RO_SOURCE = False
SET_WARN_TIME_REMAINING_SEC = 9000
SET_CRIT_TIME_REMAINING_SEC = 1800

#############################################################################
# MAIN

def main(args):
    
    ##########################################################################################################################################
    # SCRIPT STARTUP

    logmessage("**************************************************************************************",logfile,1)
    logmessage("*                                                                                    *",logfile,1)
    logmessage("* DerushCX Script                                                                    *",logfile,1)
    logmessage("*    Automates extraction of rushes marked though the GoPro 'hilight'                *",logfile,1)
    logmessage("*    feature in a set of video files                                                 *",logfile,1)
    logmessage("*                                                                                    *",logfile,1)
    logmessage("*   Version 1.0 - 08/10/2023 - Initial Python version                                *",logfile,1)
    logmessage("*   Version 1.1 - 06/11/2023 - Added messages (summary, file count...)               *",logfile,1)
    logmessage("*   Version 1.2 - 13/11/2023 - Added ensure_path function to enhance path handleling *",logfile,1)
    logmessage("*   Version 1.3 - 02/12/2023 - Added printing of space/time left on source           *",logfile,1)
    logmessage("*                            - Enhanced printing of durations                        *",logfile,1)
    logmessage("*   Version 1.4 - 19/01/2025 - Handles metadata errors                               *",logfile,1)
    logmessage("*                                                                                    *",logfile,1)
    logmessage("*                                                                                    *",logfile,1)
    logmessage("*                                                 <!            @                    *",logfile,1)
    logmessage("*                                                  !___   _____`\<,!_!               *",logfile,1) 
    logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/ (*)                *",logfile,1)
    logmessage("*  https://gitlab.com/cycliste-urbain/resources                                      *",logfile,1)
    logmessage("**************************************************************************************",logfile,1)
    logmessage("Log file : "+logfile,logfile,1)

    #Flags and other variables init
    FLG_do_not_write_db = False
    average_bitrate = None

    ####
    # CHECKING ARGUMENTS -----------------------------------------------------------------------------------------------------------------------
    ####

    logmessage("Checking input and ouput path arguments...",logfile,1)

    #Source dir
    if(os.path.isdir(args.source)):
        source_dir = ensure_path(args.source)
    else:
        logmessage("  Source dir '"+args.source+"' is not a valid path",logfile,5)
        return(False)

    #Output dir
    if(os.path.isdir(args.output)):
        output_dir = ensure_path(args.output)
    else:
        logmessage("  Output dir '"+args.output+"' is not a valid path",logfile,5)
        return(False)

    ####
    # READING DB FILE -----------------------------------------------------------------------------------------------------------------------
    ####

    logmessage("Checking if source directory is writable (for db)...",logfile,1)

    #It's not writable but --ignore-ro-source was provided -> continue but raises the FLG_do_not_write_db flag
    if(not os.access(source_dir, os.W_OK) and args.ignore_ro_source):
            logmessage("  Source directory is not writable but --ignore-ro-source argument was provided",logfile,3)
            FLG_do_not_write_db = True

     #It's not writable -> Exit script        
    elif(not os.access(source_dir, os.W_OK) and not args.ignore_ro_source):
            logmessage("  Source directory is not writable. Consider using --ignore-ro-source argument if it is expected",logfile,5)
            return(False)    
    
    if(not args.ignore_db):
        logmessage("Reading .db file in source dir "+source_dir,logfile,1)
        try:
            with open(source_dir+SET_DEFAULT_DB_NAME, 'r') as dbf:
                extracted_files = dbf.read()
                dbf.close()
        except FileNotFoundError as oops:
            if(FLG_do_not_write_db):
                logmessage("  DB file "+oops.filename+" is not present, but it won't be created anyway as source dir is not writable",logfile,3)
            else:
                logmessage("  DB file "+oops.filename+" is not present, it will be created",logfile,3)
            extracted_files = ""
    else:
        logmessage("Existing "+SET_DEFAULT_DB_NAME+" file will be ignored with --ignore-db argument. As a result, previous extracted will be overwritten!",logfile,3)
        extracted_files = ""


    ####
    # SETTINGS SUMMARY ----------------------------------------------------------------------------------------------------------------------
    ####

    logmessage("Ready to process files : ",logfile,1)
    logmessage("    Source directory : "+source_dir,logfile,1)
    logmessage("    Output directory : "+output_dir,logfile,1)
    logmessage("    Seconds before HiLight : "+str(args.before)+"s",logfile,1)
    logmessage("    Seconds after HiLight : "+str(args.after)+"s",logfile,1)
    logmessage("    Min time between two HiLights : "+str(args.min_delta)+"s",logfile,1)
    logmessage("    Output file prefix : '"+args.file_prefix+"'",logfile,1)
    logmessage("    Output file suffix : '"+args.file_suffix+"'",logfile,1)

    ####
    # PROCESSING VIDEO FILES -----------------------------------------------------------------------------------------------------------------
    ####
    total_hilights = 0
    total_hilights_ignored = 0
    total_hilights_ok = 0
    total_hilights_error = 0
    total_files = 0
    total_files_ignored = 0
    total_files_processed = 0 
    db_new_filelist = [] #New list of files to be written in DB

    global_chronostart = datetime.now()

    logmessage("Looping through files in source directory",logfile,1)

    video_files = [f for f in pathlib.Path(source_dir).glob("G*.MP4")]
    logmessage(str(len(video_files))+" files found",logfile,1)

    for file in video_files:
        filepath = source_dir+file.name
        filename = file.name

        total_files += 1

        #Checks that file is not in db
        if(filename in extracted_files and not args.ignore_db):
            logmessage("Skipping file "+filename+" as it has already been processed",logfile,1)
            total_files_ignored += 1
            #We still add it to new db
            db_new_filelist.append(filename)
            continue

        logmessage("Processing file "+filename+" ("+str(total_files)+"/"+str(len(video_files))+")",logfile,1)
        total_files_processed += 1

        ##
        # Reads metadata ---------
        
        logmessage("  Reading metadata",logfile,1)
        #Sad but true, ffmpeg-python's probe method doesn't read chapters... we need to use a subprocess execution of ffprobe -show_chapters
        try:
            command = ['ffprobe', '-v', 'error','-show_format', '-show_chapters', '-print_format', 'json', filepath]
            output = sp.check_output(command, stderr=sp.STDOUT, universal_newlines=True,text=True)
            filemetadata = json.loads(output)
        except Exception as oops:
            logmessage("  Could not read the file's metadata : "+format(oops),logfile,4)
            continue
        try:
            video_creation_time = datetime.strptime(filemetadata['format']['tags']['creation_time'],"%Y-%m-%dT%H:%M:%S.%fZ") #2023-10-04T18:11:23.000000Z
            video_duration = int(float(filemetadata['format']['duration']))
            video_filesize = int(filemetadata['format']['size'])
            video_bitrate = int(filemetadata['format']['bit_rate'])
            video_firmware = filemetadata['format']['tags']['firmware']
            video_camera_model = int(video_firmware.split('.')[0][2:])
            video_fw_version = video_firmware[len(video_firmware.split('.')[0])+1:]
        except KeyError as oops : 
            logmessage("  File is missing some required metadata, it might be broken : "+format(oops),logfile,4)
            continue

        #Calculates the average bitrate 
        if(average_bitrate is None):
            average_bitrate = video_bitrate
        else:
            average_bitrate = (average_bitrate+video_bitrate)/2

        #For files created with GoPro Hero 8 or newer video_creation_time needs to be ajusted on secondary files as it's set as the first file's creation time
        if(video_camera_model >= 8 and int(filename[2:4]) > 1):
            logmessage("  "+filename+" is a secondary file from a Hero8 or newer camera. Creation time information needs to be corrected",logfile,1)
            filesbefore = int(filename[2:4]) - 1
            #Gets the duration of the first file
            firstfilepath = source_dir+filename[0:2]+"01"+filename[4:]
            try:
                command = ['ffprobe', '-v', 'error','-show_format', '-print_format', 'json', firstfilepath]
                output = sp.check_output(command, stderr=sp.STDOUT, universal_newlines=True,text=True)
                firstfilemetadata = json.loads(output)
            except Exception as oops:
                logmessage("  Could not read the file's metadata : "+format(oops),logfile,4)
                continue
            firstfileduration= int(float(firstfilemetadata['format']['duration']))
            #Calculates the actual start of the current file, based on the cumulated duration of the files before it
            startoffset=filesbefore * firstfileduration
            logmessage("  "+filename+"'s creation date is shifted "+str(startoffset)+" seconds",logfile,1)
            video_creation_time = video_creation_time + timedelta(seconds=startoffset)
        
        #Now we can print the metadata
        durationstr = str(timedelta(seconds=video_duration))
        
        logmessage("   Captured on : "+video_creation_time.strftime("%d/%m/%Y at %H:%M:%S"),logfile,1)
        logmessage("   Duration    : "+durationstr,logfile,1)
        logmessage("   Bitrate     : "+str(video_bitrate),logfile,1)
        logmessage("   Firmware    : "+video_firmware,logfile,1)

        ##
        # Loops through HiLights if there was some -------------

        if(len(filemetadata['chapters']) > 0):
            
            #Quick summary
            logmessage("   HiLights    : "+str(len(filemetadata['chapters'])),logfile,1)
            for hl in filemetadata['chapters']:
                hlminutes, hlseconds = divmod(float(hl['start_time']), 60)
                if(hlseconds<10):
                    hlsecondsstr = "0"+str(int(hlseconds))
                else:
                    hlsecondsstr = str(int(hlseconds))
                hlstr = str(int(hlminutes))+":"+str(hlsecondsstr)+"s"
                logmessage("        at "+hlstr,logfile,1)

            #HiLight extration
            logmessage("  Extracting "+str(len(filemetadata['chapters']))+" hilight(s)",logfile,1)
            hilight_count = 0
            hilight_ok = 0
            hilight_last  = None #Previous processed hilight position (for ignoring feature)

            hilights_chronostart = datetime.now() 

            for hilight in filemetadata['chapters']:
                total_hilights += 1
                hilight_count +=1

                hilight_position = round(float(hilight['start_time']))

                #Ignores if to close from previous
                if(not hilight_last is None and (hilight_position - hilight_last)< args.min_delta):
                    logmessage("  Ignoring HiLight #"+str(hilight_count)+" as it's "+str(hilight_position - hilight_last)+"s from previous",logfile,3)
                    total_hilights_ignored += 1
                
                #Extraction process
                else:
                    logmessage("  Extracting HiLight #"+str(hilight_count)+" at "+str(hilight_position)+"s...",logfile,1)
                    #Define start and duration of extraction
                    extract_start = hilight_position - int(args.before)
                    if(extract_start < 0):
                        extract_start = 0
                    extract_duration = int(args.before)+int(args.after)               

                    #Filename with date/time of start
                    extract_filename = args.file_prefix+(video_creation_time + timedelta(seconds=extract_start)).strftime("%Y%m%d_%H%M%S")+args.file_suffix+".MP4" 

                    #Extraction
                    try:
                        command = ['ffmpeg', '-loglevel', 'fatal','-ss', str(extract_start), '-t', str(extract_duration), '-i', filepath, '-c', 'copy', output_dir+extract_filename, '-y']
                        output = sp.check_output(command, stderr=sp.STDOUT, universal_newlines=True,text=True)
                        logmessage("  Successfully extracted as "+extract_filename,logfile,2)
                        total_hilights_ok +=1
                        hilight_ok += 1
                    except Exception as oops:
                        logmessage("  Extraction failed : "+format(oops)+"/"+output,logfile,4)
                        total_hilights_error +=1
                    
                    hilight_last = hilight_position

            #It's over for that file 
            hilights_chronostop = datetime.now()
            hilights_chronotime = hilights_chronostop-hilights_chronostart
            extracttimestr = str(timedelta(seconds=int(hilights_chronotime.total_seconds())))

            logmessage("Extracted "+str(hilight_ok)+" hilight(s) in "+extracttimestr,logfile,2)

        #No HiLight in file
        else:
            logmessage("   HiLights    : None",logfile,1)
        
        #Adds the filename to DB list
        db_new_filelist.append(filename+"\n")

    #All files were processed

    global_chronostop = datetime.now()
    global_chronotime = global_chronostop-global_chronostart
    globaltimestr = str(timedelta(seconds=int(global_chronotime.total_seconds())))

    logmessage("Processed "+str(total_files_processed)+" files of "+str(total_files)+" in "+globaltimestr+" (ignored "+str(total_files_ignored)+")",logfile,1)

    if(not FLG_do_not_write_db):
        logmessage("Writing .db file as "+source_dir+SET_DEFAULT_DB_NAME,logfile,1)
        try:
            with open(source_dir+SET_DEFAULT_DB_NAME, 'w') as dbf:
               dbf.writelines(db_new_filelist)
               dbf.close()
        except Exception as oops:
            logmessage("  Could not write db file : "+format(oops),logfile,4)
    
    #Show remaining time if possible
    source_total, source_used, source_free = shutil.disk_usage(source_dir)
    source_pct_free = str(round(source_free/source_total,2)*100)+"%"
    source_gb_free = str((source_free // (2**30)))+"GB"
    

    if(average_bitrate is None):
        logmessage("Cannot estimate remaining time on card as no file were processed",logfile,3)
        logmessage("Free space on source dir : "+source_gb_free+" ("+source_pct_free+")",logfile,1)
    else:
        average_mbps = round(average_bitrate/1000000,0)
        average_byterate = int(round(average_bitrate/8,0))
        seconds_remaining = int(round(source_free/average_byterate,0))
        time_remaining = timedelta(seconds=seconds_remaining)
        if(seconds_remaining <= SET_CRIT_TIME_REMAINING_SEC):
            level = 4
        elif(seconds_remaining <= SET_WARN_TIME_REMAINING_SEC):
            level = 3
        else:
            level = 2
        logmessage(source_gb_free+" free on source ("+source_pct_free+"), that is "+str(time_remaining)+" at "+str(average_mbps)+"Mbps",logfile,level)

    
    ##########################################################################################################################################
    #END MAIN
    return (True)

#############################################################################
# RUN
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'DerushCX.py : Extracts parts from GoPro generated video files based on the hilight feature')
    parser.add_argument('--source', '-s', help="Directory that contains source files", type= str, default=SET_DEFAULT_SOURCE_DIR)
    parser.add_argument('--output', '-o', help="Directory where files will be generated", type= str, default=SET_DEFAULT_OUTPUT_DIR)
    parser.add_argument('--before', '-b', help="Seconds to extract before the hilight", type= int, default=SET_DEFAULT_SECONDS_BEFORE)
    parser.add_argument('--min-delta', '-m', help="Minimum number of seconds between two hilights", type= int, default=SET_DEFAULT_MIN_DELTA)
    parser.add_argument('--after', '-a', help="Seconds to extract after the hilight", type= int, default=SET_DEFAULT_SECONDS_AFTER)
    parser.add_argument('--file-prefix', '-fp', help="Prefix of the generated files' names", type= str, default=SET_DEFAULT_FILE_PREFIX)
    parser.add_argument('--file-suffix', '-fs', help="Suffix of the generated files' names", type= str, default=SET_DEFAULT_FILE_SUFFIX)
    parser.add_argument('--ignore-db','-i', help="Ignores the db file that lists files that were already extracted, implies overwritting previous generated files",action='store_true', default=SET_DEFAULT_IGNORE_DB)
    parser.add_argument('--ignore-ro-source','-r', help="Ignores if source directory is read only and db file cannot be written",action='store_true', default=SET_DEFAULT_IGNORE_RO_SOURCE)
    parser.add_argument('--log', '-l', help="Log file location", type= str, default=SET_DEFAULT_LOG_DIR)

    args = parser.parse_args()

    logfile = logmessage("Let's go!")
    execresult = main(args=args)
    if(execresult):
        logmessage("Script ended with success",logfile,2)
    else:
        logmessage("Script ended with error",logfile,4)