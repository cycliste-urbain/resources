#!/usr/bin/python3

##############################################
# IMPORTS

import argparse
import sys
import locale
from datetime import datetime, timedelta
from time import strftime
from dateutil.relativedelta import relativedelta
import pytz
import json

from mastodon import Mastodon

import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import seaborn
from sqlalchemy import false; seaborn.set()

from PIL import Image, ImageDraw, ImageFont

##############################################
# FUNCTIONS

#Ajout de la date depuis date_time
def add_date(row):
    return row['date_time'].date()

#Ajout d'un bool "Jour de semaine" depuis date
def add_isweekday(row):
    return int(datetime.strftime(row['date'],"%u"))<=5

#Ajout de l'heure en mode unitaire depuis date_time
def add_hour(row):
    return int(row['date_time'].hour)

#Ajout de l'année depuis date
def add_year(row):
    return row['date'].year

#Ajout du jour dans l'année depuis date
def add_yeardaynumber(row):
    return int(datetime.strftime(row['date'],"%j"))


##############################################
# SETTINGS AND INIT

#Importing config file
import xxxx_stats_config as config
from xxxx_stats_functions import *

set_logpath = config.LOGPATH

locale.setlocale(locale.LC_TIME,'fr_FR.utf8')
locale_tz = pytz.timezone("Europe/Paris")
logfile = None

#Pixel size
px = 1/plt.rcParams['figure.dpi']

#From config file
EVO_TOLERANCE = config.EVO_TOLERANCE
MAX_TOOT_SIZE = config.MAX_TOOT_SIZE
MAX_COUNTERS = config.MAX_COUNTERS_MS
LIST_SEPARATOR = config.LIST_SEPARATOR
USE_SYSLOG = config.USE_SYSLOG

MS_API_URL = config.PRS_MS_API_URL
MS_API_TOKEN = config.PRS_MS_API_TOKEN

IMG_TEMPDIR = config.IMG_TEMPDIR
IMG_PNG_QUALITY = config.IMG_PNG_QUALITY
GRAPH_HEIGHT = config.GRAPH_HEIGHT
GRAPH_WIDTH = config.GRAPH_WIDTH
GRAPH_TITLE_FONTSIZE = config.GRAPH_TITLE_FONTSIZE
GRAPH_AXES_FONTSIZE = config.GRAPH_AXES_FONTSIZE

MAP_IMG_FILE = config.PRS_MAP_IMG_FILE
MAP_BOUNDINGDATA_FILE = config.PRS_MAP_BOUNDING
MAP_MARKER = config.MAP_MARKER
MAP_MARKER_SIZE = config.MAP_MARKER_SIZE
MAP_BOX_MARGIN = config.MAP_BOX_MARGIN
MAP_BOX_FONT_FILE = config.MAP_BOX_FONT_FILE
MAP_BOX_TITLE_FONTSIZE = config.MAP_BOX_TITLE_FONTSIZE
MAP_BOX_TEXT_FONTSIZE = config.MAP_BOX_TEXT_FONTSIZE
MAP_BOX_TITLE_BG = config.MAP_BOX_TITLE_BG
MAP_BOX_TEXT_BG = config.MAP_BOX_TEXT_BG


flag_counter_is_over_one_year = False

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Script "+os.path.basename(main.__file__)+" starting up with "+str(len(sys.argv)-1)+" argument(s) "+(' '.join(sys.argv[1:])),None,1,USE_SYSLOG)

logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* paris_stats_random_counter_toot                                                    *",logfile,2)
logmessage("*    Toots about a random counter for Paris                                          *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*   Version 1.0 - 10/03/2024 - Initial version                                       *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1)

global_chronostart = datetime.now()
##############################################
# ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--tootit','-t', help="Switch pour indiquer que l'on pouette le résultat",action='store_true', default=False)
parser.add_argument('--resetlist','-r', help="Switch pour indiquer que l'on réinitialise la liste aléatoire des compteurs",action='store_true', default=False)
parser.add_argument('--noupdate','-n', help="Switch pour indiquer que l'on ne met pas à jour la liste aléatoire des compteurs",action='store_true', default=False)
args = parser.parse_args()

#Do we reset or not?
reset_list = args.resetlist
if(reset_list):
    logmessage("Reset actif : la liste aléatoire des compteurs sera réinitialisée",logfile,3,USE_SYSLOG)

#Do we update or not?
noupdate = args.noupdate
if(noupdate):
    logmessage("Mode No Update : la liste aléatoire des compteurs ne sera pas mise à jour",logfile,3,USE_SYSLOG)

#Do we toot or not?
toot_it = args.tootit
if(toot_it):
    logmessage("Mode TootIt actif : le resultat sera publié dans un pouet",logfile,2,USE_SYSLOG)
else:
    logmessage("Mode TootIt inactif : le resultat ne sera pas poueté",logfile,3,USE_SYSLOG)


#Initialisation de l'objet mastodon
if(toot_it):
    logmessage("Initialisation de l'objet Mastodon",logfile,1,USE_SYSLOG)
    mastodon = Mastodon(access_token = MS_API_TOKEN ,api_base_url=MS_API_URL)

##############################################
# GESTION DE LA LISTE DES COMPTEURS

#Récupère la liste existante
logmessage("Récupération de la liste aléatoire des compteurs",logfile,1,USE_SYSLOG)
randomized_counter_list_str = get_persistent_data('randomized_counter_list')
randomized_counter_list = list(filter(None,randomized_counter_list_str.split(LIST_SEPARATOR)))
logmessage("   >"+str(len(randomized_counter_list))+" élément(s) dans la liste",logfile,2,USE_SYSLOG)

#Initialisation ou réinitialisation de la liste
if(len(randomized_counter_list)==0 or reset_list):
    logmessage("(Ré)initialisation de la liste",logfile,1,USE_SYSLOG)
    
    try:
        query  = "SELECT nom_axe_abrg from compteurs WHERE ville='Paris' and Actif = 1 GROUP BY nom_axe_abrg"
        sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
        dbConnection = sqlEngine.connect()
        data = pd.read_sql(text(query),dbConnection)
        dbConnection.close()
        sqlEngine.dispose()
        logmessage("  >"+str(len(data))+" compteurs présents pour construire la liste",logfile,2,USE_SYSLOG)
    except Exception as oops:
        logmessage("   Impossible de récupérer la liste des compteurs : "+format(oops),logfile,5,USE_SYSLOG)
        quit()

    #Brassage aléatoire de la liste
    random.shuffle(data['nom_axe_abrg'])
    randomized_counter_list = data['nom_axe_abrg'].tolist()
    randomized_counter_list_str = (LIST_SEPARATOR).join(randomized_counter_list)
    logmessage("  >Liste brassée : "+randomized_counter_list_str,logfile,0,USE_SYSLOG)

    #Sauvegarde en base
    logmessage("Enregistrement de la nouvelle liste aléatoire",logfile,1,USE_SYSLOG)
    if(set_persistent_data('randomized_counter_list',randomized_counter_list_str)):
        logmessage("  >OK, nouvelle liste enregistrée",logfile,2)
    else:
        logmessage("  >La nouvelle liste n'a pu être enregistrée. Vérifiez que la clé 'randomized_counter_list' est présente dans la table des données persistantes",logfile,4,USE_SYSLOG)

#On verifie que le compteur a des données sur l'année en cours
flag_counter_has_data_this_year = False

while(flag_counter_has_data_this_year == False):
    #Extrait le premier item de la liste
    target_counter = randomized_counter_list.pop(0)
    logmessage("Compteur cible du toot : "+target_counter,logfile,1,USE_SYSLOG)

    #Sauvegarde la liste sans le compteur cible
    if(not(noupdate)):
        logmessage("Enregistrement de la liste mise à jour",logfile,1,USE_SYSLOG)
        randomized_counter_list_str = (LIST_SEPARATOR).join(randomized_counter_list)

        if(set_persistent_data('randomized_counter_list',randomized_counter_list_str)):
            logmessage("  >OK, nouvelle liste enregistrée",logfile,2)
        else:
            logmessage("  >La nouvelle liste n'a pu être enregistrée. Vérifiez que la clé 'randomized_counter_list' est présente dans la table des données persistantes",logfile,4,USE_SYSLOG)

    #Verifie que le compteur a des données sur cette année
    logmessage("Vérification que le compteur a des données sur l'année en cours",logfile,1,USE_SYSLOG)
    if(sql_get_counter_count_between_dates(target_counter,datetime(datetime.now().year,1,1),datetime.now()) > 0 ):
        flag_counter_has_data_this_year = True
        logmessage("Le compteur a des données, on continue avec lui",logfile,2)
    else:
        logmessage("Le compteur n'a pas de données sur l'année en cours, on tente le suivant",logfile,3)

######################################################
# INFORMATIONS SUR LE COMPTEUR
try:
    logmessage("Récupération des informations du compteur",logfile,1,USE_SYSLOG)
    infos_compteur = sql_get_compteur_infos(target_counter)
    logmessage("  OK",logfile,2)
except Exception as oops:
    logmessage("  Impossible de récupérer les infos du compteur: "+format(oops),logfile,5,USE_SYSLOG)
    quit()


######################################################
# INITIALISATION DES DATAFRAMES DES DONNEES DE COMPTAGE

logmessage("Recupération des données de comptage du compteur "+target_counter,logfile,1,USE_SYSLOG)
try:
    chronostart = datetime.now()
    data = sql_get_counter_fullhistory(target_counter)
    if(len(data)>0):
        chronotime = datetime.now() - chronostart
        logmessage(strs(len(data))+" enregistrements récupérés en "+strs(round(chronotime.total_seconds(),2))+"s",logfile,2,USE_SYSLOG)
    else:
        logmessage("  La requête n'a remonté aucune information",logfile,5,USE_SYSLOG)
        quit()

except Exception as oops:
    logmessage("  Impossible de récupérer les données du compteur : "+format(oops),logfile,5,USE_SYSLOG)
    quit()

#Ajout d'une colonne "date" depuis "date_time"
data['date'] = data.apply(add_date, axis = 1)

#Aggregation pour les manipulation par heure
logmessage("Aggregation des données par heure",logfile,1,USE_SYSLOG)
try: 
    comptages_heure = (data.pivot_table(values='comptage',index='date_time',aggfunc='sum')).reset_index()
    comptages_heure['date']      = comptages_heure.apply(add_date, axis = 1)
    comptages_heure['isWeekDay'] = comptages_heure.apply(add_isweekday, axis = 1)
    comptages_heure['heure']     = comptages_heure.apply(add_hour, axis = 1)
    comptages_heure['annee']     = comptages_heure.apply(add_year, axis = 1)
except Exception as oops:
    logmessage("  Impossible d'aggreger les données du compteur : "+format(oops),logfile,5,USE_SYSLOG)
    quit()

#Aggregation pour les manipulations par jour
logmessage("Aggregation des données par jour",logfile,1,USE_SYSLOG)
try:
    comptages_jour = (data.pivot_table(values='comptage',index='date',aggfunc='sum')).reset_index()
    comptages_jour['isWeekDay'] = comptages_jour.apply(add_isweekday, axis = 1)
    comptages_jour['annee'] = comptages_jour.apply(add_year, axis = 1)
    comptages_jour['jour'] = comptages_jour.apply(add_yeardaynumber, axis = 1)
except Exception as oops:
    logmessage("  Impossible d'aggreger les données du compteur : "+format(oops),logfile,5,USE_SYSLOG)
    quit()

######################################################
# CALCUL DES INDICATEURS

#Nombre total de passages depuis l'installation
total_ever = np.sum(comptages_heure['comptage'])
date_installation = np.min(data['date_installation'])
logmessage("Total : "+strs(total_ever)+" depuis le "+datetime.strftime(date_installation,"%A %e %B %Y"),logfile,1,USE_SYSLOG)

#Nombre total de passages cette année (ou la dernière)
premier_janvier =  datetime(np.max(comptages_jour['date']).year,1,1).date()
sub = comptages_jour[comptages_jour['date']>=premier_janvier]
total_annee = np.sum(sub['comptage'])

#Moyenne jour globale
moyenne_jour = np.average(comptages_jour['comptage'])
logmessage("Moyenne globale: "+strs(round(moyenne_jour)),logfile,1,USE_SYSLOG)

#Moyennes par jour de semaine et en week-end
moyenne_jour_semaine = np.average(comptages_jour[comptages_jour['isWeekDay']==True]['comptage'])
moyenne_jour_we = np.average(comptages_jour[comptages_jour['isWeekDay']==False]['comptage'])
logmessage("Moyenne en semaine : "+strs(round(moyenne_jour_semaine))+" / Moyenne le weekend : "+strs(round(moyenne_jour_we)),logfile,1,USE_SYSLOG)

#Evolution de la moyenne globale
date_max = np.max(comptages_jour['date'])
date_min = datetime(date_max.year,1,1).date()
date_max_1yb = date_max - relativedelta(years=1)
date_min_1yb = datetime(date_max_1yb.year,1,1).date()

sub = comptages_jour[comptages_jour['date']<=date_max]
sub = sub[sub['date']>=date_min]
sub_1yb = comptages_jour[comptages_jour['date']<=date_max_1yb]
sub_1yb = sub_1yb[sub_1yb['date']>=date_min_1yb]

if(len(sub_1yb)>0):
    flag_counter_is_over_one_year = True

    #Nombre de passages sur la même periode l'année précédente
    total_annee_prec = np.sum(sub_1yb['comptage'])

    #On ne calcule les évolutions que si l'on a des données sur l'année précédente
    moyenne_annee = np.average(sub['comptage'])
    moyenne_annee_prec = np.average(sub_1yb['comptage'])
    evo_moyenne = (moyenne_annee - moyenne_annee_prec)/moyenne_annee_prec

    if(evo_moyenne > 0):
        evo_moyenne_sign = "+"
    else:
        evo_moyenne_sign = ""
    evo_moyenne_txt = evo_moyenne_sign+str(round(100*evo_moyenne,2))+"%"
    logmessage("Moyenne cette année: "+str(round(moyenne_annee))+" / Moyenne l'année précédente: "+str(round(moyenne_annee_prec))+" soit "+evo_moyenne_txt,logfile,1,USE_SYSLOG)

    #Evolution de la moyenne en semaine
    moyenne_annee_sem = np.average(sub[sub['isWeekDay']==True]['comptage'])
    moyenne_annee_prec_sem = np.average(sub_1yb[sub_1yb['isWeekDay']==True]['comptage'])
    evo_moyenne_sem = (moyenne_annee_sem - moyenne_annee_prec_sem)/moyenne_annee_prec_sem

    if(evo_moyenne_sem > 0):
        evo_moyenne_sem_sign = "+"
    else:
        evo_moyenne_sem_sign = ""
    evo_moyenne_sem_txt = evo_moyenne_sem_sign+str(round(100*evo_moyenne_sem,2))+"%"
    logmessage("Moyenne en semaine cette année: "+str(round(moyenne_annee_sem))+" / l'année précédente: "+str(round(moyenne_annee_prec_sem))+" soit "+evo_moyenne_sem_txt,logfile,1,USE_SYSLOG)

    #Evolution de la moyenne en week-end
    moyenne_annee_we = np.average(sub[sub['isWeekDay']==False]['comptage'])
    moyenne_annee_prec_we = np.average(sub_1yb[sub_1yb['isWeekDay']==False]['comptage'])
    evo_moyenne_we = (moyenne_annee_we - moyenne_annee_prec_we)/moyenne_annee_prec_we

    if(evo_moyenne_we > 0):
        evo_moyenne_we_sign = "+"
    else:
        evo_moyenne_we_sign = ""
    evo_moyenne_we_txt = evo_moyenne_we_sign+str(round(100*evo_moyenne_we,2))+"%"
    logmessage("Moyenne le week-end cette année: "+str(round(moyenne_annee_we))+" / l'année précédente: "+str(round(moyenne_annee_prec_we))+" soit "+evo_moyenne_we_txt,logfile,1,USE_SYSLOG)
else:
    logmessage("Le compteur n'a pas de données sur l'année précédente",logfile,3,USE_SYSLOG)

#Jour record
record = comptages_jour.sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
record_jour = record['date'][0]
record_jour_comptage = int(record['comptage'][0])
cyclistes_par_minutes = round(record_jour_comptage / (24*60),1)
logmessage("Record : "+strs(record_jour_comptage)+" passages le "+datetime.strftime(record_jour,"%A %e %B %Y")+" soit "+str(cyclistes_par_minutes)+" cyclistes/min",logfile,1,USE_SYSLOG)

#Heure Record
record = comptages_heure.sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
record_heure_jour = record['date_time'][0].date()
record_heure_1 = record['date_time'][0].hour
record_heure_2 = (record['date_time'][0] + relativedelta(hours=+1)).hour
record_heure_comptage = int(record['comptage'][0])
record_heure_frequence = round(60*60/record_heure_comptage,1)
logmessage("Heure record : "+str(record_heure_1)+"h-"+str(record_heure_2)+"h, le "+datetime.strftime(record_heure_jour,"%A %e %B %Y")+" : "+strs(record_heure_comptage)+" passages, soit un.e cycliste toutes les "+str(record_heure_frequence)+" secondes",logfile,1,USE_SYSLOG)

#Jour record cette annee
comptages_jour_cette_annee = comptages_jour[comptages_jour['date']>=date_min]
record_cette_annee = comptages_jour_cette_annee.sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
record_jour_cette_annee = record_cette_annee['date'][0]
record_jour_comptage_cette_annee = int(record_cette_annee['comptage'][0])
cyclistes_par_minutes_cette_annee = round(record_jour_comptage_cette_annee / (24*60),1)
logmessage("Record cette année: "+strs(record_jour_comptage_cette_annee)+" passages le "+datetime.strftime(record_jour_cette_annee,"%A %e %B %Y")+" soit "+str(cyclistes_par_minutes_cette_annee)+" cyclistes/min",logfile,1,USE_SYSLOG)

#Heure Record cette annee
comptages_heure_cette_annee = comptages_heure[comptages_heure['date']>=date_min]
record_cette_annee = comptages_heure_cette_annee.sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
record_heure_jour_cette_annee = record_cette_annee['date_time'][0].date()
record_heure_1_cette_annee = record_cette_annee['date_time'][0].hour
record_heure_2_cette_annee = (record_cette_annee['date_time'][0] + relativedelta(hours=+1)).hour
record_heure_comptage_cette_annee = int(record_cette_annee['comptage'][0])
record_heure_frequence_cette_annee = round(60*60/record_heure_comptage_cette_annee,1)
logmessage("Heure record cette année: "+str(record_heure_1_cette_annee)+"h-"+str(record_heure_2_cette_annee)+"h, le "+datetime.strftime(record_heure_jour_cette_annee,"%A %e %B %Y")+" : "+strs(record_heure_comptage_cette_annee)+" passages, soit un.e cycliste toutes les "+str(record_heure_frequence_cette_annee)+" secondes",logfile,1,USE_SYSLOG)



#Heure de pointe en semaine
moyenne_heure = (comptages_heure[comptages_heure['isWeekDay']].pivot_table(values='comptage',index='heure',aggfunc='mean')).reset_index()
hp_matin = moyenne_heure[moyenne_heure['heure']<12].sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
hp_matin_heure = hp_matin['heure'][0]
hp_matin_moyenne = int(round(hp_matin['comptage'][0],0))
hp_soir = moyenne_heure[moyenne_heure['heure']>=12].sort_values('comptage',axis=0,ascending=False)[0:1].reset_index()
hp_soir_heure = hp_soir['heure'][0]
hp_soir_moyenne = int(round(hp_soir['comptage'][0],0))
logmessage("Heure de pointe du matin : "+str(hp_matin_heure)+"h-"+str(hp_matin_heure+1)+"h, avec "+strs(hp_matin_moyenne)+" passages en moyenne",logfile,1,USE_SYSLOG)
logmessage("Heure de pointe du soir : "+str(hp_soir_heure)+"h-"+str(hp_soir_heure+1)+"h, avec "+strs(hp_soir_moyenne)+" passages en moyenne",logfile,1,USE_SYSLOG)

######################################################
# CARTE

# Chargement de la carte
logmessage("Génération de la carte du compteur",logfile,1,USE_SYSLOG)
err_counter_map = False
try:
    logmessage("  Chargement des metadonnées de la carte",logfile,1,USE_SYSLOG)
    f = open(MAP_BOUNDINGDATA_FILE)
    map_bounding_data = json.load(f)
    f.close()
    # Convertit la carte en un tableau NumPy
    map_array = plt.imread(MAP_IMG_FILE, format="P")
    width, height = map_array.shape[1], map_array.shape[0]

except Exception as oops:
    logmessage("  Impossible de charger les informations de la carte: "+format(oops),logmessage,4,USE_SYSLOG)
    err_counter_map = True

#Récupérations des métadonnées de tous les compteurs
logmessage("  Récupérations des données des compteurs",logfile,1,USE_SYSLOG)
try:
    all_counter_metadata = sql_get_compteurs('Paris',False,True)
except Exception as oops:
    logmessage("  Impossible de charger les informations des compteurs: "+format(oops),logmessage,4,USE_SYSLOG)
    err_counter_map = True

#Création de la carte sous forme de graphique
if(not err_counter_map):
    try:
        logmessage("  Génération du graphique de la carte",logfile,1,USE_SYSLOG)
        fig, ax = plt.subplots(figsize=(16, 12))
        ax.imshow(map_array, extent=[0, width, 0, height])

        # Ajoute un marqueur pour chaque compteur
        for index, row in all_counter_metadata.iterrows():
            nom_axe = row["nom_axe"]
            longitude = row["longitude_compteur"]
            latitude = row["latitude_compteur"]

            x = int((longitude - map_bounding_data['longitude-min']) * map_array.shape[1] / (map_bounding_data['longitude-max'] - map_bounding_data['longitude-min']))
            y = int((latitude - map_bounding_data['latitude-min']) * map_array.shape[0] / (map_bounding_data['latitude-max'] - map_bounding_data['latitude-min']))
            
            # Adds markers enphasing the focused one
            if(row["nom_axe_abrg"] == target_counter):
                ax.plot([x, x], [y, y + 1], MAP_MARKER, markersize=MAP_MARKER_SIZE,alpha=0.9)
                target_x = x
                target_y = y
            else:
                ax.plot([x, x], [y, y + 1], 'bo', markersize=MAP_MARKER_SIZE-10,alpha=0.5)
                
        # Add labels and limits
        plt.xlim(0, 1200)
        plt.ylim(0, 900)
        plt.xticks([])
        plt.yticks([])

        # Save the map to a PNG file
        extent = ax.get_tightbbox().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(IMG_TEMPDIR+"map_tmp.png", bbox_inches=extent)
    except Exception as oops:
        logmessage("  Impossible de générer le graphique de la carte: "+format(oops),logmessage,4,USE_SYSLOG)
        err_counter_map = True

     # Ajout du masque de la carte
    try:
        logmessage("  Ajout du masque de la carte",logfile,1,USE_SYSLOG)
        im = Image.open(IMG_TEMPDIR+"map_tmp.png")

        #Creates an transparent PNG the size of the graph
        bg = Image.new(mode="RGBA", size=im.size)

        #Creates a mask
        mask = Image.new("L", im.size, 0)
        draw = ImageDraw.Draw(mask)
        draw.rounded_rectangle((2, 2, im.size[0]-2, im.size[1]-2), fill=255, radius=30)

        image = Image.composite(im, bg, mask)
    except Exception as oops:
        logmessage("  Impossible de générer le masque de la carte: "+format(oops),logmessage,4,USE_SYSLOG)
        err_counter_map = True

# Ajout du rectangle d'infos
if(not err_counter_map):
    logmessage("  Ajout du rectangle d'infos",logfile,1,USE_SYSLOG)

    nom_axe = infos_compteur['nom_axe'].values[0]
    moyenne_annee = np.average(sub['comptage'])

    image_width  = image.size[0]
    image_height = image.size[1]

    last_point_x = target_x * image_width/map_array.shape[1]
    last_point_y = image_height-(target_y * image_height/map_array.shape[0]) #Graph Y-axis is inverted

    draw = ImageDraw.Draw(image)

    # Prépare le titre
    if(len(nom_axe)>=60):
        lb_pos = nom_axe.find(" ",int(len(nom_axe)/2))
        text_title = (nom_axe[0:lb_pos],nom_axe[lb_pos+1:])
    else:
        text_title = (nom_axe,"")
    
    # Prépare le texte
    text_lines = (
        strs(total_ever)+" passages depuis le "+datetime.strftime(date_installation,"%e %B %Y"),
        "Moyenne de "+strs(round(moyenne_jour))+" passages/jour",
        "Record le "+datetime.strftime(record_jour,"%e %B %Y")+" avec "+strs(record_jour_comptage)+" passages"
        )
    
    # Définit la largeur du texte du rectangle
    maxcar = 0
    for line in text_title:
        maxcar = max(maxcar,len(line))
    for line in text_title:
        maxcar = max(maxcar,len(line))

    car_title_w = MAP_BOX_TITLE_FONTSIZE * 0.55
    car_title_h = MAP_BOX_TITLE_FONTSIZE * 0.90

    title_width  =  int(maxcar * car_title_w)

    # Définit la hauteur du texte du rectangle
    maxcar = 0
    for line in text_lines:
        maxcar = max(maxcar,len(line))
    for line in text_title:
        maxcar = max(maxcar,len(line))

    car_text_w = MAP_BOX_TEXT_FONTSIZE * 0.55
    car_text_h = MAP_BOX_TEXT_FONTSIZE * 1.05

    text_width  =  int(maxcar * car_text_w)
    text_height = len(text_title)*car_title_h + len(text_lines)*car_text_h

    # Calcul des dimensions du rectangle
    box_width  =  int(max(title_width,text_width))
    box_height =  int(text_height + 10)

    # Définit l'emplacement du rectangle
    if(last_point_x > image_width / 2):
        #Seconde moitié, le rectangle est à gauche
        box_x = last_point_x - box_width - MAP_BOX_MARGIN
    else:
        #Première moitié, le rectangle est à droite
        box_x = last_point_x + MAP_BOX_MARGIN

    if(last_point_y > 2 * image_height/3):
        #Tiers inférieur, le rectange est en haut
        box_y = last_point_y - box_height - MAP_BOX_MARGIN
    elif(last_point_y < image_height/3):
        #Tiers supérieur, le rectange est en bas
        box_y =  last_point_y + MAP_BOX_MARGIN
    else:
        #Tiers central, le rectangle est centré
        box_y =  last_point_y - box_height/2

    # Rectangles arrondis de titre et texte
    radius = 20
    draw.rounded_rectangle((box_x, box_y, box_x+box_width, box_y+box_height), radius, fill=MAP_BOX_TEXT_BG, outline="black")                      #Boite
    draw.rounded_rectangle((box_x, box_y, box_x+box_width, box_y+(len(text_title)*car_title_h)), radius, fill=MAP_BOX_TITLE_BG, outline="black")  #Titre


    # Ajoute le titre '/home/c0re/scripts/data/Millimetre-Regular.otf'
    font = ImageFont.truetype(MAP_BOX_FONT_FILE, MAP_BOX_TITLE_FONTSIZE)  
    texte = "\n".join(text_title)
    draw.text((box_x + 10, box_y + 10), texte, fill="white", font=font)

    # Ajoute le texte
    shift_y = len(text_title)*car_title_h+5 # On décale arbitrairement de 5px vers le bas
    font = ImageFont.truetype(MAP_BOX_FONT_FILE, MAP_BOX_TEXT_FONTSIZE)  
    texte = "\n".join(text_lines)
    draw.text((box_x + 10, shift_y + box_y), texte, fill="white", font=font)

    # Ajoute une signature
    font = ImageFont.truetype(MAP_BOX_FONT_FILE, 14)  
    text = "Généré par @parisvelobot@boitam.eu le "+datetime.strftime(datetime.now(),"%d/%m/%Y")
    draw.text((20, image_height -20 ), text, fill="white", font=font)

    # Sauvegarde l'image
    map_img_outfile = IMG_TEMPDIR+"map_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("  Enregistrement de l'image en tant que '"+map_img_outfile,logfile+"'",1,USE_SYSLOG)
    try:
        image = image.convert("P", palette=Image.ADAPTIVE, colors=256)
        image.save(map_img_outfile)
        logmessage("  Fichier créé avec succès",logfile,2,USE_SYSLOG)
    except Exception as oops:
        logmessage("  Impossible de sauvegarder le fichier : "+format(oops),logfile,4,USE_SYSLOG)
        err_counter_map = True

######################################################
# GRAPHIQUES

# Graphique du trafic par année
logmessage("Génération du graph de trafic par année",logfile,1,USE_SYSLOG)

#Ajout de la MMC7J et troncature
for i in range(0,comptages_jour.shape[0]-6):
    comptages_jour.loc[comptages_jour.index[i+3],'MMC7'] = np.round(((comptages_jour.iloc[i,1]+ comptages_jour.iloc[i+1,1] +comptages_jour.iloc[i+2,1]+comptages_jour.iloc[i+3,1]+comptages_jour.iloc[i+4,1]+comptages_jour.iloc[i+5,1]+comptages_jour.iloc[i+6,1])/7),1)
data_annees = comptages_jour[3:]

data_annees = data_annees.pivot_table('MMC7', index='jour', columns='annee')
data_annees = data_annees.reset_index()

columns_temp = []
for i in data_annees.columns:
    columns_temp.append(str(i))
data_annees.columns=columns_temp

#couleurs
line_colors = [
    [1,0.733,0.494],
    [1,0.663,0.365],
    [0.824,0.506,0.22],
    [0.49,0.275,0.082],
    [0.192,0.11,0.035]
]

#graphique
fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)

plt.rcParams['xtick.major.size'] = 5
plt.rcParams['xtick.major.width'] = 2
plt.rcParams['xtick.bottom'] = True
plt.rcParams['ytick.left'] = True
#grille
ax.grid(visible=True, which='major', color='w', linewidth=1.0)
ax.grid(visible=True, which='minor', color='w', linewidth=0.5)

if(len(data_annees.columns)<6): #Si on a moins de 5 années d'historique
    first_year = 1
else:
    first_year = len(data_annees.columns)-5
i=0
for year in data_annees.columns[first_year:len(data_annees.columns)]:
    ax.plot('jour', year, data=data_annees, linewidth=3, color=line_colors[i])
    i+=1

# Major ticks every month, minor ticks every week,
ax.xaxis.set_major_locator(mdates.MonthLocator())
#grille
ax.grid(visible=True, which='major', color='w', linewidth=1.0)
ax.grid(visible=True, which='minor', color='w', linewidth=0.5)
ax.set_ylabel('Nombre de passages', fontsize=GRAPH_AXES_FONTSIZE)
ax.legend(fontsize=GRAPH_AXES_FONTSIZE)
ax.set_title('Comparaison du trafic vélo annuel pour le compteur '+infos_compteur['nom_axe'].values[0], fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})

ax.xaxis.set_major_formatter(mdates.DateFormatter('      %b'))
# Rotates and right-aligns the x labels so they don't crowd each other.
for label in ax.get_xticklabels(which='major'):
    label.set(rotation=0, horizontalalignment='left', fontsize=GRAPH_AXES_FONTSIZE)
for label in ax.get_yticklabels(which='major'):
    label.set(fontsize=GRAPH_AXES_FONTSIZE-4)

#Enregistrement de l'image
compteur_annuel_img_filepath = IMG_TEMPDIR+"ms_compteur_annuel_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
logmessage("  Enregistrement de l'image en tant que "+compteur_annuel_img_filepath,logfile,1,USE_SYSLOG)
try:
    plt.savefig(compteur_annuel_img_filepath)
    logmessage(" Fichier créé avec succès",logfile,2,USE_SYSLOG)
    err_graph_compteur_annuel = False
except Exception as oops:
    logmessage(" Impossible de sauvegarder le fichier : "+format(oops),logfile,4,USE_SYSLOG)
    err_graph_compteur_annuel = True


###
# POUET PRINCIPAL
############################################################
logmessage("Preparation du premier pouet",logfile,1)

#Ligne d'introduction
toot_text  = "Le compteur de la semaine: "+nom_axe+"\r\n\r\n"
#Infos générales
toot_text  += emoji.ROCKT+" Depuis le "+datetime.strftime(date_installation,"%A %e %B %Y")+ ", il a compté "+strs(total_ever)+" cyclistes\r\n"
toot_text  += emoji.CLNDR+" En moyenne, il en voit passer "+strs(round(moyenne_jour_semaine))+" par jour la semaine et "+strs(round(moyenne_jour_we))+" le week-end\r\n"
toot_text  += emoji.FIRE+" Ses heures de pointe sont "+str(hp_matin_heure)+"h-"+str(hp_matin_heure+1)+"h le matin ("+strs(hp_matin_moyenne)+" en moyenne) et "+str(hp_soir_heure)+"h-"+str(hp_soir_heure+1)+"h le soir ("+strs(hp_soir_moyenne)+")\r\n"

#Records
toot_text  += emoji.TADA+" Son record a été le "+datetime.strftime(record_jour,"%A %e %B %Y")+" avec "+strs(record_jour_comptage)+", soit "+str(cyclistes_par_minutes)+" "+emoji.CYCLST+"/min et la meilleure heure a été "+str(record_heure_1)+"h-"+str(record_heure_2)+"h le "+datetime.strftime(record_heure_jour,"%A %e %B %Y")+" avec "+strs(record_heure_comptage)+" passages, soit un.e "+emoji.CYCLST+" toutes les "+str(record_heure_frequence)+" secondes"+emoji.HDBLW+"\r\n"
toot_text  += emoji.DOWN

#Resumé du toot : 
logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
    #Toot It!
    if(toot_it):
        attachment_ids = []
        try:
            #Si on a pu génrer l'image de la carte
            if(not err_counter_map):
                try:
                    attachment_alt = "Carte de Paris et de ses compteurs vélo, avec le compteur "+nom_axe+" mis en évidence. A côté de ceui-ci un cadre qui indique son nom et les informations suivantes : "+" / ".join(text_lines)
                    logmessage("Envoi de l'image "+map_img_outfile+" avec le ALT "+attachment_alt,logfile,1)                    
                    attachment = mastodon.media_post(media_file=map_img_outfile,description=attachment_alt)
                    attachment_id = str(attachment.id)
                    logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                    attachment_ids.append(attachment_id)
                except Exception as oops:
                    logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text,media_ids=attachment_ids)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"


###
# POUET DE REPONSE AVEC LES INFOS DE L'ANNEE
############################################################
logmessage("Préparation du pouet de réponse des infos de l'année en cours",logfile,1)
reply_to_toot = toot_id

toot_text  = "Cette année, il a compté "+strs(total_annee)+" cyclistes.\r\n"

#Seulement si on a une comparaison avec l'année précédente
if(flag_counter_is_over_one_year):

    #Progression
    if(evo_moyenne > EVO_TOLERANCE):
        evo_text = emoji.INCR+" Il progresse par rapport à "+str(date_max_1yb.year)+" avec "+evo_moyenne_txt
    elif(evo_moyenne < -EVO_TOLERANCE):
        evo_text = emoji.DECR+" Il regresse par rapport à "+str(date_max_1yb.year)+" avec "+evo_moyenne_txt
    else:
        evo_text = emoji.STABLE+" Il reste stable par rapport à "+str(date_max_1yb.year)+" ("+evo_moyenne_txt+")"

    toot_text += evo_text

    toot_text += "\r\n\r\nLa moyenne des jours de semaine passe de "+strs(round(moyenne_annee_prec_sem))+" à "+strs(round(moyenne_annee_sem))+" ("+evo_moyenne_sem_txt+") " 
    toot_text += "et celle des jours de week-end de "+strs(round(moyenne_annee_prec_we))+" à "+strs(round(moyenne_annee_we))+" ("+evo_moyenne_we_txt+")\r\n"

#Records de l'année
toot_text  += emoji.TADA+" Le meilleur jour a été le "+datetime.strftime(record_jour_cette_annee,"%A %e %B")+" avec "+strs(record_jour_comptage_cette_annee)+", soit "+str(cyclistes_par_minutes_cette_annee)+" cyclistes/min et la meilleure heure a été "+str(record_heure_1_cette_annee)+"h-"+str(record_heure_2_cette_annee)+"h le "+datetime.strftime(record_heure_jour_cette_annee,"%A %e %B")+" avec "+strs(record_heure_comptage_cette_annee)+" passages, soit un.e cycliste toutes les "+str(record_heure_frequence_cette_annee)+" secondes "+emoji.HDBLW

#Resumé du toot : 
logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)

    #Toot It!
    if(toot_it):
        attachment_ids = []
        try:
            #Si on a pu génrer l'image du graphique
            if(not err_graph_compteur_annuel):
                try:
                    attachment_alt = "Graphique de comparaison annuelle du trafic du compteur "+nom_axe
                    logmessage("Envoi de l'image "+compteur_annuel_img_filepath+" avec le ALT "+attachment_alt,logfile,1)                    
                    attachment = mastodon.media_post(media_file=compteur_annuel_img_filepath,description=attachment_alt)
                    attachment_id = str(attachment.id)
                    logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                    attachment_ids.append(attachment_id)
                except Exception as oops:
                    logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text,media_ids=attachment_ids,in_reply_to_id=reply_to_toot)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"

###
# SCRIPT ENDING
############################################################

global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,1)
logmessage("Script "+os.path.basename(main.__file__)+" ending after "+exectimestr,logfile,2,USE_SYSLOG)