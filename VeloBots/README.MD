# Les VeloBots

Les _Velobots_ représentent un lot de scripts Python qui chargent des données de comptages de passages vélo disponibles en OpenData et les restituent après calculs sur des comptes Mastodon et Twitter.
Ils sont actuellement deux : 
- **Montreuil Velo Bot**, qui récupère les données de la plateforme OpenData de Montreuil et les publie sur [Mastodon (@montreuilvelobot@boitam.eu)](https://boitam.eu/@montreuilvelobot) et [Twitter (@montreuilvelo)](https://twitter.com/MontreuilVelo)
- **Paris Velo Bot**, qui récupère les données de la plateforme OpenData de Paris et les publie sur [Mastodon (@parisvelobot@boitam.eu)](https://boitam.eu/@parisvelobot)

Je publie ici les informations sur et les scripts utilisés par ces Bot d'une part pour des raisons de transparence sur les statistiques calculées, et d'autre part à des fins de révision des calculs statistiques par la communauté.

Les informations détaillées de chacun sont disponibles dans les ReadMe : 
- [VeloBots/README_MontreuilVeloBot.MD](README_MontreuilVeloBot.MD)
- [VeloBots/README_ParisVeloBot.MD](README_ParisVeloBot.MD)

## Calcul des statistiques et choix stratégiques

_Les scripts calculent un certain nombre de statistiques. Si certaines sont claires comme les comptages d'un jour J, d'autres ont du faire l'objets de choix stratégiques pour garantir la fourniture d'informations pertinentes._ 
### Evolution du trafic journalier sur un an
Le calcul de l'évolution de date à date sur un an, s'il est factuellement juste, n'est pas pertinant du fait de la volatilité du trafic selon les jours de la semaine. Ainsi, comparer le trafic du 24 octobre 2022, un lundi, à celui du 24 octobre 2021, un dimanche, n'a pas de sens. 

Le choix s'est donc porté sur la comparaison du trafic du jour de l'année A à la moyenne mobile sur 7 jours du trafic, centrée sur la date identique de l'année A-1. Ainsi, pour le 24 octobre 2022, on compare le trafic du jour à la moyenne du trafic journalier entre les 21 et 27 octobre 2021. Ceci permet d'effectuer une comparaison plus pertinente car lissée sur la semaine entière. 

### Evolution sur un mois
Afin de déterminer une tendance proche de l'évolution du trafic, cette mesure calcule la moyenne du trafic vélo des trois jours identiques précédents et la compare au trafic du jour. Par exemple, on compare le trafic du 24 octobre 2022 à la moyenne du trafic sur les lundis 3, 10 et 17 octobre 2022.

### Gestion de la disponibilité des compteurs
Il arrive, ponctuellement ou sur des périodes plus longue comme celles de travaux sur la voirie, que certains compteurs parisiens ne remontent pas d'informations. Hélas, le nombre de compteurs rend impossible un suivi fin de leurs états. En cas d'indisponibilité, ils ne sont donc pas indiqués dans les statistiques unitaires, mais restent cependant comptabilisés dans les statitiques globales. C'est un choix effectué considérant que l'absence de remontée d'information d'un compteur n'infulence pas de façon significative les données globales, aucun compteur ne représentant plus de 10% de du trafic à l'échelle de la ville. En cas de problème plus large comme l'absence de remontée d'un compteur de premier plan (Sébastopol, Rivoli, Magenta...) ou l'absence de remontée d'un lot important de compteurs, une rectification à postériori pourra être effectuée aboutissant potentiellement à une republication des pouets correspondants
Dans le cas de Montreuil, le nombre de compteurs relativement limité permet de suivre et de définir si un compteur est actif. Un compteur inactif est donc exclu de toutes les statistiques : il n'apparait pas dans les informations unitaires et il n'est pas comptabilisé dans les évolutions.

### Gestion des évolutions abérantes
Certains compteurs peuvent remonter des évolutions du trafic mensuel d'une année sur l'autre abérante. Le seul est fixé à +300% (configurable), et le compteur dans ce cas n'est pas mentionnés dans les statistiques unitaires.

### Temps et définition des jours de pluie
- Les informations météo sont récupérées depuis le site https://open-meteo.com/. Les requêtes sur des dates dans les 5 derniers mois utilisent les données Meteo France (https://api.open-meteo.com/v1/meteofrance), celles sur ces dates plus anciennes utilisent les données en archive (https://archive-api.open-meteo.com/v1/archive)
- Le temps de la journée indiqué dans le pouet Mastodon est défini par le code WMO (World Meteorological Organization) de la journée, récupéré de l'API. L'information n'étant pas toujours exacte, une évolution est prévue pour prendre, à la place, le code WMO ayant le plus grand nombre d'occurences dans la journée.
- Une journée est considéré pluvieuse à partir du moment où il y a eu au moins 0.3mm de précipitations sur la période 7h-20h (configurable)
- Une heure est considérée pluvieuse à partir de 0.1mm de précipitations (configurable)
- Les graphiques qui affichent un dégradé de couleur selon les précipitation ont un plafond à 10mm/jour (configurable). Les jours dont la pluviométrie dépasse ce plafond afficheront tous la couleur la plus haute du dégradé. C'est bien en deça du record parisien d'environ 100mm en 12h, mais celà permet d'avoir des dégradés plus étalés en dehors des situations exceptionnelles.

## Elements techniques communs
Certains éléments techniques sont communs aux deux bots : la configuration, les fonctions, la base de données ainsi que l'outil de cache.

### Fichier de configuration *xxxx_stats_config.py*
C'est ici que l'on retrouve la configuration des bots : coordonnées de la base de données, clés et URL d'API etc... Certains éléments sont identifiés en "Legacy" afin d'assurer une compatibilité avec les premiers scripts créés du temps où seul le Montreuil Vélobot était prévu.

### Fichier de fonctions *xxxx_stats_functions.py*
C'est un fichier de fonctions communes aux deux bots. Il contient des fonctions séparées en trois groupes : 
- Les fonctions et classes générales utilisées pour la journalisation, les émoji, la gestion des couleurs etc...
- Les fonctions d'ajout de colonnes dans des datafames, appelée par la méthode apply
- Les fonctions de récupération de données dans la base de données

### Structure de la base de données *database_structure.sql*
C'est un fichier modèle pour la base de données utilisée. Il ne contient que la structure, sans les informations de compteur ou de datasets.

### Script de mise en cache *xxxx_stats_update_cache.py*
Ce script permet de gérer la mise en cache dans des tables du contenu de vues spécifiées. 

Les noms vues doivent commencer par *vw_* ou *ctl_*, et une table d'une structure equivalente à la vue, nommée *cache_nom_sans_le_prefixe* doit exister dans la base. (Par exemple : *vw_compteurs_montreuil* -> *cache_compteurs_montreuil*)
La vue doit également être incluse dans la liste *CACHE_SUPPORTED_VIEWS* de la configuration.

Le script dispose d'un paramètre -v permettant de spécifier les vues à cacher, séparées par un espace. Si le paramètre est omis, l'ensemble des vues indiquées dans la configuration sont mises en cache.

La structure de la base de données et la configuration permettent de base la mise en cache de 4 vues : 
- vw_comptages_date (agregation par date des comptages)
- ctl_comptage_montreuil_update (dernières données disponibles pour chaque compteur de Montreuil)
- ctl_comptage_paris_update (dernièrs données disponibles pour chaque compteur de Paris)
- ctl_compteurs_paris_manquants (compteurs de Paris non référencés en base de donnée)
A chacune correspond une table *cache_* (ex : cache_comptages_date) qui est peuplée à l'execution du script. 


[_bsky.app/profile/cyclisteurbain.bsky.social_](https://bsky.app/profile/cyclisteurbain.bsky.social)
[_boitam.eu/@cyclisteurbain_](https://boitam.eu/@cyclisteurbain)

      <!            @
       !___   _____`\<,!_!
       !(*)!--(*)---/(*)

Made in Montreuil with :peach: