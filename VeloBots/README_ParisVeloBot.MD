# Paris Velo Bot
_Le bot qui publie des statistiques sur le trafic vélo sur les compteurs de Paris sur Mastodon_

**https://boitam.eu/@parisvelobot**

## Contexte
La ville de Paris met à disposition sur sa plateforme OpenData les données de comptage de vélos issues de la centaine de compteurs installés sur certains de ces axes cylables.

Ce bot consiste en l'execution quotienne de deux scripts : 
- Un premier script qui charge les données de comptages de la veille dans une base de données
- Un second qui extrait des données de cette base et publie des statistiques basées sur ces informations sur le compte Mastodon [@parisvelobot@boitam.eu](https://boitam.eu/@parisvelobot)

Depuis le mois d'Avril 2023 s'ajoute un script qui s'exécute mensuellement, le 4 de chaque mois, et publie des statistiques sur le mois précédent (uniquement sur Mastodon).

[_@CyclisteUrbain_](https://twitter.com/CyclisteUrbain) 

## Source des données et processus ETL
Les données contenant les passages par heure sont récupérées quotidiennement via l'api du site https://opendata.paris.fr/ sur le dataset [comptage-velo-donnees-compteurs](https://opendata.paris.fr/explore/dataset/comptage-velo-donnees-compteurs/information/?disjunctive.id_compteur&disjunctive.nom_compteur&disjunctive.id&disjunctive.name&sort=date) 

Elles sont ensuite intégrées de façon brutes, modulo l'ajout d'une référence de compteur interne, dans une base de données MySQL qui les mettra à disposition du script de publication. 
Un hash MD5 calculé à partir d'une concaténation de l'horaire et de l'identifiant interne du compteur, et inséré dans un champ à valeur unique permet de garantir l'absence de doublon en cas de chargement multiples.

Tous les lundis, une récupération complète des données du mois en cours est effectuée pour compléter les enregistrements manquants. Tous les 2 du mois, une récupération complète des données du mois précédent est faite pour compléter les enregistrement manquants en prévision du Toot mensuel du 4.

<div style="text-align: center;">--------------------------------------------------------------------</div>

## paris-stats-load.py
_Script effectuant le chargement en base de données d'un dataset pour une plage de dates donnée._

### Usage
paris-stats-load.py -d|--dataset *dataset*  [-s|--startdate *date_depart*] [-e|--enddate *date_fin*] [--fastinsert|-f]
- [-d|--dataset *dataset*] : Spécifie le dataset à charger depuis l'API, via son id en base de données. Par défaut, il s'agit de *comptage-velo-donnees-compteurs*
- [-s|--startdate *date_depart*] : Première date à charger, au format JJ/MM/AAAA. Par défaut, il s'agit de la veille.
- [-e|--enddate *date_fin*] : Dernière date à charger, au format JJ/MM/AAAA. Par défaut, c'est la date *startdate* qui est utilisée
- [-f|--fastinsert] : Switch qui déclenche une insertion en base en lots par date. Elle implique un chargement plus rapide mais également l'échec de toute la requête pour la date en cas de doublon.
- [-m|--fullmonth] : Switch qui force le chargement des données de tout le mois de --startdate, ou, si ce de dernier est omis, de tout le mois en cours
- [-p|--previousmonth] : Switch qui spécifie que les données à charger sont celles du mois précédent. Ecrase les paramètres startdate et enddate s'ils sont spécifiés.  

### Versions/changelog

**1.6 (25/11/2023)**
- Ajout de la possibilité de journalisation Syslog

**1.5 (17/09/2023)**
- Ajout des switchs -previousmonth et -fullmonth, qui permetent de charger les données d'un mois entier. Associé à une execution sans fastinsert, l'utilisation typique est de réconcillier les enregistrements de tout un mois en récupérant ceux qui manquent.

**1.4b (08/04/2023)**
- Utilisation des fichiers de configuration et de fonctions communs
- Suppression des modules inutilisés

**1.4 (25/02/2023)**
- Ajout du switch fastinsert qui, s'il n'est pas actif, engendre un chargement enregistrement par enregistrement. Ceci permet d'inserer après coup les données d'un compteur manquant lors d'un import précédent en évitant l'échec de l'insertion par lot.

**1.3 (07/01/2023)**
- Version initiale sur base monteuil-stats-load
- Utilisation d'un fichier de fonctions
- Utilisation de Pandas et SQL Alchemy

<div style="text-align: center;">--------------------------------------------------------------------</div>

## paris-stats-daily-toot.py
_Script de publication quotidienne d'un pouet Mastodon_

### Usage
paris-stats-daily-toot [-d|--date *date*]  [-t|--tootit]
- [-d|--date *date*] : Indique la date sur laquelle publier les statistiques. Par défaut, il s'agit de la veille.
- [-t|--tootit] : Switch qui indique si l'on publie ou non le Pouet

### Versions/changelog

**1.6 (07/06/2024)**
- Le pouet de record affiche désormais, pour les records compteurs, le comptage du jour et non seulement le record précédent battu. 

**1.5 (25/11/2023)**
- Ajout de la possibilité de journalisation Syslog
- Prise en charge des erreurs dues à des données horaires manquantes 

**1.4 (15/05/2023)**
- Le temps de la journée est défini par le code WMO majoritaire sur la plage identifiée comme journée et non plus par le code renvoyé par l'API sur le jour entier

**1.3 (13/05/2023)**
- En cas d'échec d'upload des images, de nouvelles tentatives sont effectuées
- Mise à jour de la mécanique de recherche des records de compteur, ce qui permet de ne plus se limiter au top 5

**1.2 (18/03/2023)**
- Utilisation des fonctions communes au bot Paris Velo Bot
- Ajout des données météo calculées depuis Open Weather API
- Modification des tailles de graphiques
- Exclusion des compteurs identifiés comme non actif

**1.1 (08/03/2023)**
- Utilisation des fichiers de configuration et de fonctions communs
- Récupération d'infos météo
- Ajout des données météo au pouet et aux graphiques

**1.0 (09/01/2023)**
- Version initale


<div style="text-align: center;">--------------------------------------------------------------------</div>

## paris-stats-monthly-toot.py
_Script de publication mensuelle d'un pouet Mastodon_

### Usage
paris-stats-monthly-toot.py [-d|--date *date*]  [-t|--tootit]
- [-d|--date *date*] : Date dans le mois pour lequel on traite les statistiques Par défaut, il s'agit du premier jour du mois précédent
- [-t|--tootit] : Switch qui indique si l'on publie ou non le Pouet

### Versions/changelog

**1.0 (18/03/2023)**
- Version intiale


<div style="text-align: center;">--------------------------------------------------------------------</div>

## paris-stats-randomcounter-toot.py
_Script de publication d'un pouet Mastodon de statistiques d'un compteur au hasard_

Entretien une liste brassée des compteurs et en sort un de celle-ci à chaque exécution pour en publier des statistiques accompagnées d'une carte de Paris pointant sa localisation. Si le compteur n'a pas de données sur l'année en cours, il tente le suivant.

### Usage
paris-stats-randomcounter-toot.py [-t|--tootit] [-r|--resetlist] [-n|--noupdate]
- [-t|--tootit] : Switch qui indique si l'on publie ou non le Pouet
- [-r|--resetlist] : Reinitialise la liste brassée des compteurs
- [-n|--noupdate] : Ne met pas à jour la liste brassée

### Versions/changelog

**1.0 (10/03/2024)**
- Version intiale


[_twitter.com/CyclisteUrbain_](https://twitter.com/CyclisteUrbain)
[_boitam.eu/@cyclisteurbain_](https://boitam.eu/@cyclisteurbain)

      <!            @
       !___   _____`\<,!_!
       !(*)!--(*)---/(*)

Made in Montreuil with :peach:
