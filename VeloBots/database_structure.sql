-- phpMyAdmin SQL Dump
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dw_compteursvelo`
--
CREATE DATABASE IF NOT EXISTS `dw_compteursvelo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `dw_compteursvelo`;

-- --------------------------------------------------------

--
-- Structure de la table `cache_comptages_date`
--

DROP TABLE IF EXISTS `cache_comptages_date`;
CREATE TABLE `cache_comptages_date` (
  `id_compteur` varchar(32) NOT NULL,
  `id_site` int NOT NULL,
  `id_dataset` varchar(255) NOT NULL,
  `nom_compteur` varchar(128) NOT NULL,
  `nom_site` varchar(128) NOT NULL,
  `nom_axe` varchar(128) NOT NULL,
  `nom_axe_court` varchar(64) NOT NULL,
  `nom_axe_abrg` varchar(32) DEFAULT NULL,
  `nom_complet` varchar(255) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `arrondissement` int NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `flux` varchar(16) NOT NULL,
  `actif` tinyint DEFAULT NULL COMMENT '1',
  `date` date DEFAULT NULL,
  `comptage` decimal(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cache_comptage_montreuil_update`
--

DROP TABLE IF EXISTS `cache_comptage_montreuil_update`;
CREATE TABLE `cache_comptage_montreuil_update` (
  `Compteur` varchar(128) NOT NULL,
  `Latest` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cache_comptage_paris_update`
--

DROP TABLE IF EXISTS `cache_comptage_paris_update`;
CREATE TABLE `cache_comptage_paris_update` (
  `Axe` varchar(128) NOT NULL,
  `Compteur` varchar(128) NOT NULL,
  `ID` varchar(32) NOT NULL,
  `Latest` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cache_compteurs_paris_manquants`
--

DROP TABLE IF EXISTS `cache_compteurs_paris_manquants`;
CREATE TABLE `cache_compteurs_paris_manquants` (
  `id_compteur` varchar(32) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `comptages`
--

DROP TABLE IF EXISTS `comptages`;
CREATE TABLE `comptages` (
  `id` bigint NOT NULL,
  `id_record` varchar(40) NOT NULL,
  `date_time` datetime NOT NULL,
  `comptage` int NOT NULL,
  `id_compteur` varchar(32) NOT NULL,
  `id_dataset` varchar(255) NOT NULL,
  `record_timestamp` datetime NOT NULL,
  `checksum` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `compteurs`
--

DROP TABLE IF EXISTS `compteurs`;
CREATE TABLE `compteurs` (
  `id_compteur` varchar(32) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `nom_compteur` varchar(128) NOT NULL,
  `id_site` int NOT NULL,
  `nom_site` varchar(128) NOT NULL,
  `nom_axe` varchar(128) NOT NULL,
  `arrondissement` int NOT NULL,
  `flux` varchar(16) NOT NULL,
  `date_installation` datetime NOT NULL,
  `url_photo` varchar(255) DEFAULT NULL,
  `latitude_compteur` float NOT NULL,
  `longitude_compteur` float NOT NULL,
  `latitude_axe` float NOT NULL,
  `longitude_axe` float NOT NULL,
  `nom_axe_court` varchar(64) NOT NULL,
  `nom_axe_abrg` varchar(32) DEFAULT NULL,
  `nom_complet` varchar(255) NOT NULL,
  `actif` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `ctl_comptage_montreuil_update`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `ctl_comptage_montreuil_update`;
CREATE TABLE `ctl_comptage_montreuil_update` (
`Compteur` varchar(128)
,`Latest` datetime
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `ctl_comptage_paris_update`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `ctl_comptage_paris_update`;
CREATE TABLE `ctl_comptage_paris_update` (
`Axe` varchar(128)
,`Compteur` varchar(128)
,`ID` varchar(32)
,`Latest` datetime
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `ctl_compteurs_paris_manquants`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `ctl_compteurs_paris_manquants`;
CREATE TABLE `ctl_compteurs_paris_manquants` (
`date` date
,`id_compteur` varchar(32)
);

-- --------------------------------------------------------

--
-- Structure de la table `datasets`
--

DROP TABLE IF EXISTS `datasets`;
CREATE TABLE `datasets` (
  `id_dataset` varchar(128) NOT NULL,
  `ville` varchar(32) DEFAULT NULL,
  `api_root` varchar(255) NOT NULL,
  `countfield1` varchar(64) DEFAULT NULL,
  `countfield2` varchar(64) DEFAULT NULL,
  `id_compteur1` varchar(32) DEFAULT NULL,
  `id_compteur2` varchar(32) DEFAULT NULL,
  `last_record_timestamp` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `persistent_data`
--

DROP TABLE IF EXISTS `persistent_data`;
CREATE TABLE `persistent_data` (
  `id_data` bigint NOT NULL,
  `data_key` varchar(32) NOT NULL,
  `data_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vw_comptages_date`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `vw_comptages_date`;
CREATE TABLE `vw_comptages_date` (
`actif` tinyint(1)
,`arrondissement` int
,`comptage` decimal(32,0)
,`date` date
,`flux` varchar(16)
,`id_compteur` varchar(32)
,`id_dataset` varchar(255)
,`id_site` int
,`latitude` float
,`longitude` float
,`nom_axe` varchar(128)
,`nom_axe_abrg` varchar(32)
,`nom_axe_court` varchar(64)
,`nom_complet` varchar(255)
,`nom_compteur` varchar(128)
,`nom_site` varchar(128)
,`ville` varchar(64)
);

-- --------------------------------------------------------

--
-- Structure de la vue `ctl_comptage_montreuil_update`
--
DROP TABLE IF EXISTS `ctl_comptage_montreuil_update`;

DROP VIEW IF EXISTS `ctl_comptage_montreuil_update`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ctl_comptage_montreuil_update`  AS SELECT `cp`.`nom_compteur` AS `Compteur`, max(`ca`.`date_time`) AS `Latest` FROM (`comptages` `ca` join `compteurs` `cp` on((`cp`.`id_compteur` = `ca`.`id_compteur`))) WHERE (`cp`.`ville` = 'Montreuil') GROUP BY `Compteur` ORDER BY `Compteur` ASC ;

-- --------------------------------------------------------

--
-- Structure de la vue `ctl_comptage_paris_update`
--
DROP TABLE IF EXISTS `ctl_comptage_paris_update`;

DROP VIEW IF EXISTS `ctl_comptage_paris_update`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ctl_comptage_paris_update`  AS SELECT `cp`.`nom_axe` AS `Axe`, `cp`.`nom_compteur` AS `Compteur`, `cp`.`id_compteur` AS `ID`, max(`ca`.`date_time`) AS `Latest` FROM (`comptages` `ca` join `compteurs` `cp` on((`ca`.`id_compteur` = `cp`.`id_compteur`))) WHERE (`cp`.`ville` = 'Paris') GROUP BY `Axe`, `Compteur`, `cp`.`id_compteur` ;

-- --------------------------------------------------------

--
-- Structure de la vue `ctl_compteurs_paris_manquants`
--
DROP TABLE IF EXISTS `ctl_compteurs_paris_manquants`;

DROP VIEW IF EXISTS `ctl_compteurs_paris_manquants`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ctl_compteurs_paris_manquants`  AS SELECT `ca`.`id_compteur` AS `id_compteur`, cast(`ca`.`date_time` as date) AS `date` FROM (`comptages` `ca` left join `compteurs` `cp` on((`ca`.`id_compteur` = `cp`.`id_compteur`))) WHERE ((`ca`.`id_dataset` like 'comptage-velo-%') AND (`cp`.`id_compteur` is null)) GROUP BY `ca`.`id_compteur`, `date` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vw_comptages_date`
--
DROP TABLE IF EXISTS `vw_comptages_date`;

DROP VIEW IF EXISTS `vw_comptages_date`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_comptages_date`  AS SELECT `ca`.`id_compteur` AS `id_compteur`, `ce`.`id_site` AS `id_site`, `ca`.`id_dataset` AS `id_dataset`, `ce`.`nom_compteur` AS `nom_compteur`, `ce`.`nom_site` AS `nom_site`, `ce`.`nom_axe` AS `nom_axe`, `ce`.`nom_axe_court` AS `nom_axe_court`, `ce`.`nom_axe_abrg` AS `nom_axe_abrg`, `ce`.`nom_complet` AS `nom_complet`, `ce`.`ville` AS `ville`, `ce`.`arrondissement` AS `arrondissement`, `ce`.`latitude_axe` AS `latitude`, `ce`.`longitude_axe` AS `longitude`, `ce`.`flux` AS `flux`, `ce`.`actif` AS `actif`, cast(`ca`.`date_time` as date) AS `date`, sum(`ca`.`comptage`) AS `comptage` FROM (`comptages` `ca` join `compteurs` `ce` on((`ca`.`id_compteur` = `ce`.`id_compteur`))) GROUP BY `ca`.`id_compteur`, `ce`.`id_site`, `ca`.`id_dataset`, `ce`.`nom_compteur`, `ce`.`nom_site`, `ce`.`nom_axe`, `ce`.`nom_axe_court`, `ce`.`nom_axe_abrg`, `ce`.`nom_complet`, `ce`.`ville`, `ce`.`arrondissement`, `latitude`, `longitude`, `ce`.`flux`, `date` ORDER BY `date` DESC ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comptages`
--
ALTER TABLE `comptages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `checksum` (`checksum`);

--
-- Index pour la table `compteurs`
--
ALTER TABLE `compteurs`
  ADD PRIMARY KEY (`id_compteur`),
  ADD UNIQUE KEY `UNIQUE` (`id_compteur`);

--
-- Index pour la table `datasets`
--
ALTER TABLE `datasets`
  ADD PRIMARY KEY (`id_dataset`);

--
-- Index pour la table `persistent_data`
--
ALTER TABLE `persistent_data`
  ADD PRIMARY KEY (`id_data`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comptages`
--
ALTER TABLE `comptages`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `persistent_data`
--
ALTER TABLE `persistent_data`
  MODIFY `id_data` bigint NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
