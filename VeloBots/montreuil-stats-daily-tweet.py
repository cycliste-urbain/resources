#!/usr/bin/python3

##########################################################################################################################################
# IMPORTS
import argparse

import locale
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import pytz

import tweepy

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn; seaborn.set()

#Importing config file and functions
import xxxx_stats_config as config
from xxxx_stats_functions import *

##########################################################################################################################################
# SETTINGS AND INIT

locale.setlocale(locale.LC_TIME,'fr_FR.utf8')
locale_tz = pytz.timezone("Europe/Paris")

logfile = None
two_ways_record = True 

#Default date
default_date = datetime.strftime(datetime.now() - timedelta(days=1),"%d/%m/%Y")

#Pixel size
px = 1/plt.rcParams['figure.dpi']

#From config file
EVO_TOLERANCE = config.EVO_TOLERANCE
MAX_TWEET_SIZE = config.MAX_TWEET_SIZE
IMG_TEMPDIR = config.IMG_TEMPDIR
GRAPH_HEIGHT = config.GRAPH_HEIGHT
GRAPH_WIDTH = config.GRAPH_WIDTH
GRAPH_TITLE_FONTSIZE = config.GRAPH_TITLE_FONTSIZE

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Let's go!")

logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* montreuil-daily-stats-tweet.py                                                     *",logfile,2)
logmessage("*    Tweets daily about bike counter statistics of Montreuil                         *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*   Version 1.0 - 22/10/2022 - Initial version                                       *",logfile,2)
logmessage("*   Version 1.1 - 26/10/2022 - Adds evolution over the last month                    *",logfile,2)
logmessage("*   Version 1.2 - 26/10/2022 - Configuration transfered in a separate file           *",logfile,2)
logmessage("*   Version 1.3 - 01/11/2022 - Adds handeling of missing counters                    *",logfile,2)
logmessage("*   Version 1.3b- 06/11/2022 - Minor tweet formating update                          *",logfile,2)
logmessage("*   Version 1.5 - 22/12/2022 - Use of numpy/pandas                                   *",logfile,2)
logmessage("*                            - Change of SQL module                                  *",logfile,2)
logmessage("*                            - Use of functions for SQL queries                      *",logfile,2)
logmessage("*                            - Adds graphs to tweets                                 *",logfile,2)
logmessage("*   Version 1.5b- 08/04/2023 - Switch to common config/functions, removed unused mod *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1)


global_chronostart = datetime.now()
##########################################################################################################################################
# INTERPRETING ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--date'   ,'-d', help="Date pour laquelle on traite les statistiques. Par défaut, la veille", type= str, default=default_date)
parser.add_argument('--tweetit','-t', help="Switch pour indiquer que l'on tweete le résultat",action='store_true', default=False)
args = parser.parse_args()


#Date is an actual date
try:
    target_date = datetime.strptime(args.date,"%d/%m/%Y")
except Exception as oups:
    logmessage("La date fournie '"+args.date+"' est invalide",logfile,5)
    print(parser.format_help())
    quit()  

#Date is in the past
target_date_age = datetime.now() - target_date 
if(target_date_age.total_seconds() < 0):
    logmessage("La date fournie '"+args.date+"' est dans le futur",logfile,5)
    quit()

logmessage("Date : "+datetime.strftime(target_date,"%d-%m-%Y"),logfile,1)

#Do we tweet or not?
tweet_it = args.tweetit
if(tweet_it):
    logmessage("Mode TweetIt actif : le resultat sera publié dans un tweet",logfile,2)
else:
    logmessage("Mode TweetIt inactif : le resultat ne sera pas tweeté",logfile,3)

#Initialisation de l'objet Tweetpy
if(tweet_it):

    try:
        logmessage("Initialisation de l'objet TweetPy API (API V1) pour l'upload de medias", logfile,1)
        tw_auth = tweepy.OAuth1UserHandler(
            consumer_key=config.TW_APIKEY,
            consumer_secret=config.TW_APISECRET,
            access_token=config.TW_ACCESSTOKEN,
            access_token_secret=config.TW_ACCESSTOKENSECRET
        )
        tw_api = tweepy.API(tw_auth)

        logmessage("Initialisation de l'objet TweetPy Client (API V2) pour les tweets",logfile,1)
        #Twitter Auth
        tw_client = tweepy.Client(
            consumer_key=config.TW_APIKEY,
            consumer_secret=config.TW_APISECRET,
            access_token=config.TW_ACCESSTOKEN,
            access_token_secret=config.TW_ACCESSTOKENSECRET
        )
    except Exception as oops:
        logmessage("Impossible d'initialiser le client Twitter : "+format(oops),logfile,5)
        quit()

###
# LISTE DES SITES DE COMPTAGE
############################################################
logmessage("Base de données : liste des sites de comptage (simplifiés en 'compteurs' par la suite)",logfile,1)

try:
    compteurs = sql_get_compteurs("Montreuil")
    if(len(compteurs)==0):
        logmessage("La base de données n'a pas remonté de compteur",logfile,4)
        quit()
    logmessage("  OK : "+str(len(compteurs))+" enregistrements trouvés",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

###
# COMPTAGES DU JOUR
############################################################

# Nombre de passage par compteur
logmessage("Base de données : nombres de passages par compteur",logfile,1)

try:
    daily_counts= sql_get_city_global_daycount(target_date,"Montreuil")
    if(len(daily_counts)==0):
        logmessage("Pas de comptages pour cette date en base de données",logfile,4)
        quit()
    logmessage("  OK : "+str(len(daily_counts))+" enregistrements trouvés",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Calcul du total de la journée
logmessage("Calcul du total de la journée",logfile,1)
total_jour = np.sum(daily_counts['comptage'])
logmessage("   Total: "+str(total_jour),logfile,1)

###
# IDENTIFICATION DES RECORDS
############################################################

#Identifie si c'est un jour de record pour la ville
logmessage("Identifie si c'est un jour de record au niveau ville",logfile,1)

try:
    record_ville = sql_get_city_isrecordville(target_date,"Montreuil")
    if(record_ville):
        logmessage(" Oui! "+emoji.TADA,logfile,1)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)

#Identifie les records au niveau compteur
daily_records = []
for counter in daily_counts.index:
    logmessage("Identifie si c'est un record pour le compteur "+counter,logfile,1)

    try:
        if (sql_get_isrecordcompteur(target_date,counter)):
            logmessage(" Oui! "+emoji.TADA,logfile,1)
            daily_records.append(counter)
    except Exception as oops:
        logmessage("Echec de la requête: "+format(oops),logfile,5)

###
# MMC7J DU JOUR UN AN AVANT
############################################################
target_date_1yb = target_date - relativedelta(years=1)

logmessage("Calcul de la moyenne mobile centrée sur 7 jours de l'année précédente par compteur",logfile,1)
try:
    daily_mmc=  sql_get_city_mmc7jcompteurs(target_date_1yb,"Montreuil")
    logmessage("  OK : "+str(len(daily_mmc))+" enregistrements calculés",logfile,2)
    
    #Joins the result to the daily count DataFrame
    daily_counts = pd.concat([daily_counts, daily_mmc], axis = 1)

except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

logmessage("Calcul de la moyenne mobile centrée sur 7 jours de l'année précédente globale",logfile,1)
total_daily_mmc = np.sum(daily_counts['mmc'])
logmessage("  OK : "+str(total_daily_mmc)+" passages en moyenne",logfile,1)


###
# CALCUL EVOLUTION 1 AN
############################################################

#Evolution par compteur
logmessage("Calcul de l'évolution sur un an par compteur",logfile,1)

#Evolution en décimale
daily_counts['evo'] = daily_counts.apply(df_add_evo_column, axis = 1)
#Evolution formatée
daily_counts['evotxt'] = daily_counts.apply(df_add_evotxt_column, axis = 1)
#Evolution en emoji
daily_counts['evoemoji'] = daily_counts.apply(df_add_evoemoji_column, axis = 1)


#Evolution globale >>
logmessage("Calcul de l'évolution sur un an globale",logfile,1)
daily_evo_total = (total_jour - total_daily_mmc)/total_daily_mmc
if(daily_evo_total > 0):
    evo_sign = "+"
else:
    evo_sign = ""

#Emoji
if(daily_evo_total > EVO_TOLERANCE):
    daily_evo_total_emoji = emoji.INCR
elif(daily_evo_total < -EVO_TOLERANCE):
    daily_evo_total_emoji = emoji.DECR
else:
    daily_evo_total_emoji = emoji.STABLE

daily_evo_total_str = evo_sign+str(round(daily_evo_total*100,1))+"%"

logmessage("   Evolution globale: "+daily_evo_total_str,logfile,1)

###
# EVOLUTIONS SUR UN MOIS
############################################################

#Evolution par compteur
logmessage("Calcul de l'évolution sur un mois par compteur",logfile,1)

try:
    moyenne_mois_compteurs =  sql_get_city_moyenne3jipcompteurs(target_date,"Montreuil")
    logmessage("OK : "+str(len(moyenne_mois_compteurs))+" enregistrements calculés",logfile,2)

    #Joins the result to the daily count DataFrame
    daily_counts = pd.concat([daily_counts, moyenne_mois_compteurs], axis = 1)    

except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Evolution en décimale
daily_counts['evo3jip'] = daily_counts.apply(df_add_evo3jip_column, axis = 1)
#Evolution formatée
daily_counts['evo3jiptxt'] = daily_counts.apply(df_add_evo3jiptxt_column, axis = 1)
#Evolution en emoji
daily_counts['evo3jipemoji'] = daily_counts.apply(df_add_evo3jipemoji_column, axis = 1)

#Evolution globale

logmessage("Calcul de l'évolution sur un mois globale",logfile,1)

moyenne_mois = np.sum(daily_counts['moyenne3jip'])

logmessage(" Moyenne des trois jours identiques précédents : "+str(moyenne_mois),logfile,1)
logmessage(" Calcul de l'évolution",logfile,1)
evo_mois = (total_jour - moyenne_mois)/moyenne_mois

if(evo_mois > 0):
    evo_sign = "+"
else:
    evo_sign = ""

#Emoji
if(evo_mois > EVO_TOLERANCE):
    evo_mois_emoji = emoji.INCR
elif(evo_mois < -EVO_TOLERANCE):
    evo_mois_emoji = emoji.DECR
else:
    evo_mois_emoji = emoji.STABLE

evo_mois_str = evo_sign+str(round(evo_mois*100,1))+"%"

###
# GRAPH DE LA REPARTITON HORAIRE
############################################################
logmessage("Génération du graph de répartition horaire",logfile,1)

#Recuperation des données du graphique
try:
    repartition_horaire = sql_get_city_repartitionhoraire(target_date,"Montreuil")
    logmessage("OK : "+str(len(repartition_horaire))+" enregistrements calculés",logfile,2)
    err_grh = False
except Exception as oops:
    logmessage("Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_grh = True

if(not(err_grh)):
    logmessage("Génération du graphique",logfile,1)

    plt.rcParams["figure.figsize"] = [GRAPH_WIDTH*px, GRAPH_HEIGHT*px]
    plt.rcParams["figure.autolayout"] = True

    repartition_horaire.plot(kind='bar', stacked=True, color=config.GRAPH_COLORS)
    plt.xlabel('Heure')
    plt.ylabel('Passages')
    plt.title('Passages par heure du '+datetime.strftime(target_date,"%A %d %B %Y"),fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    plt.legend(fontsize=12)
 
    #Enregistrement de l'image
    grh_img_filepath = IMG_TEMPDIR+"tw_grh_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("Enregistrement de l'image en tant que "+grh_img_filepath,logfile,1)
    try:
        plt.savefig(grh_img_filepath)
        logmessage(" Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_grh = True

###
# GRAPH DE L'EVOLUTION SUR 3 MOIS
############################################################
logmessage("Génération du graph d'historique sur 3 mois",logfile,1)

#Recuperation des données du graphique
try:
    historique_trois_mois = sql_get_city_historique3moisville(target_date,"Montreuil",3)
    logmessage("OK : "+str(len(historique_trois_mois))+" enregistrements calculés",logfile,2)
    err_ge3m = False
except Exception as oops:
    logmessage("Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_ge3m = True

if(not(err_ge3m)):
    logmessage("Génération du graphique",logfile,1)
    #Calcul de la MMC7J de chaque entrée
    for i in range(0,historique_trois_mois.shape[0]-6):
        historique_trois_mois.loc[historique_trois_mois.index[i+3],'MMC7'] = np.round(((historique_trois_mois.iloc[i,1]+ historique_trois_mois.iloc[i+1,1] +historique_trois_mois.iloc[i+2,1]+historique_trois_mois.iloc[i+3,1]+historique_trois_mois.iloc[i+4,1]+historique_trois_mois.iloc[i+5,1]+historique_trois_mois.iloc[i+6,1])/7),1)
    #Suppression des 3 premières lignes 
    historique_trois_mois = historique_trois_mois[3:]

    #Creation du graphique
    fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)

    ax.plot('date', 'total', data=historique_trois_mois, linewidth=1, color='grey')
    ax.plot('date', 'MMC7', data=historique_trois_mois, linewidth=3, color=[0, 0.45, 0.45])
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=7))
    ax.grid(True)
    ax.set_ylabel('Nombre de passages')
    ax.set_title('Evolution des passages sur 3 mois au '+datetime.strftime(target_date,"%A %d %B %Y"), fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    # Rotates and right-aligns the x labels so they don't crowd each other.
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right')

    #Enregistrement de l'image
    ge3m_img_filepath = IMG_TEMPDIR+"tw_ge3m_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("Enregistrement de l'image en tant que "+ge3m_img_filepath,logfile,1)
    try:
        plt.savefig(ge3m_img_filepath)
        logmessage(" Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_ge3m = True


###
# TWEET PRINCIPAL
############################################################
logmessage("Preparation du premier tweet",logfile,1)

#Est-ce qu'il manque des compteurs -> on compare daily_counts avec une version filtrée des valeurs NaN dans 'comptage'.
if(len(daily_counts)>len(daily_counts.dropna(subset='comptage'))) :
    missing_counters = True
    logmessage("Attention, il manque des données de compteurs",logfile,3)
else:
    missing_counters = False

logmessage("   Informations de comptages et d'evolution sur un an",logfile,1)

target_date_str = datetime.strftime(target_date,"%A %d/%m/%Y")
previous_year   = datetime.strftime(target_date_1yb,"%Y")

tweet_text = "#Montreuil"+emoji.PEACH+": Trafic vélo du "+target_date_str+"\r\n\r\n"
tweet_text += str(total_jour)+" passages ("+daily_evo_total_emoji+" "+daily_evo_total_str+" par rapport à "+previous_year+")"

#Si certains compteurs manquent, on change les premières lignes
if(missing_counters):
    tweet_text = "#Montreuil"+emoji.PEACH+": Trafic vélo du "+target_date_str+"\r\n"
    tweet_text += str(total_jour)+" passages"
    
for compteur in compteurs['nom_axe_abrg']:
    logmessage("   Compteur : "+compteur,logfile,1)

    if(not str(daily_counts['comptage'][compteur]) == 'nan'):
        #Comptage
        compteur_comptage = round(daily_counts['comptage'][compteur])
        #Evolution
        compteur_evo = "("+ daily_counts['evotxt'][compteur] +")"
        compteur_emoji = daily_counts['evoemoji'][compteur]
        tweet_text += "\r\n"+compteur_emoji+" "+compteur+": "+str(compteur_comptage)+" "+compteur_evo
    else:
        tweet_text += "\r\n"+emoji.NOT+" "+compteur+" indisponible"     

tweet_text += "\r\n"+emoji.DOWN
logmessage("Tweet : \n\r"+tweet_text,logfile)

if(len(tweet_text) > MAX_TWEET_SIZE):
    logmessage("Longueur du tweet ("+str(len(tweet_text))+") depassant les "+MAX_TWEET_SIZE+" caracteres",logfile,4)
else:
    logmessage("Longueur du tweet ("+str(len(tweet_text))+")",logfile,1)
    #envoi du tweet
    if(tweet_it):
        attachment_ids = []
        #Upload des images

        #Image GRH
        if(not err_grh):
            try: 
                logmessage("Upload du fichier "+grh_img_filepath,logfile,1)
                attachment = tw_api.simple_upload(grh_img_filepath)
                attachment_id = attachment.media_id_string
                logmessage("  "+str(attachment.size)+" octets envoyés, ID de l'image : "+attachment_id,logfile,2)
                attachment_ids.append(attachment_id)
            except Exception as oops:
                logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

        #Image GE3M
        if(not err_ge3m):
            try:
                logmessage("Upload du fichier "+ge3m_img_filepath,logfile,1)
                attachment = tw_api.simple_upload(ge3m_img_filepath)
                attachment_id = attachment.media_id_string
                logmessage("  "+str(attachment.size)+" octets envoyés, ID de l'image : "+attachment_id,logfile,2)
                attachment_ids.append(attachment_id)
            except Exception as oops:
                logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

        try:
            logmessage("Envoi du tweet",logfile,1)
            response = tw_client.create_tweet(text=tweet_text,media_ids=attachment_ids)
            tweet_id = response.data['id']
            logmessage("Tweet envoyé avec succès. ID : "+tweet_id,logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du tweet : "+format(oops),logfile,4)
    else:
        logmessage("Tweet non envoyé (mode TweetIt inactif)",logfile,3)
        tweet_id = "0000000000000000000"


###
# TWEET DE REPONSE SUR L'EVOLUTION SUR UN MOIS
############################################################
logmessage("Preparation du tweet de reponse sur l'évolution sur un mois",logfile,1)
reply_to_tweet = tweet_id

nom_jour = datetime.strftime(target_date,"%A")

tweet_text = "Evolution sur un mois (ie par rapport aux 3 "+nom_jour+"s précédents) : "+evo_mois_emoji+" "+evo_mois_str

#Si les données de certains compteurs manquent, on remplace la première ligne
if(missing_counters):
    tweet_text = "Evolutions sur un mois (ie par rapport aux 3 "+nom_jour+"s précédents):"
     
for compteur in daily_counts.index:
    if(not str(daily_counts['comptage'][compteur]) == 'nan'):
        #Evolution
        compteur_evo = daily_counts['evo3jiptxt'][compteur]
        compteur_emoji = daily_counts['evo3jipemoji'][compteur]
        tweet_text += "\r\n"+compteur_emoji+" "+compteur+": "+compteur_evo
    else:
        tweet_text += "\r\n"+emoji.NONE+" "+compteur+": N/A" 

#Si c'est un jour de record, on annonce la réponse
if(record_ville or (len(daily_records) > 0) ):
    tweet_text += "\r\n"+emoji.DOWN

logmessage("Tweet : \n\r"+tweet_text,logfile)

if(len(tweet_text) > MAX_TWEET_SIZE):
    logmessage("Longueur du tweet ("+str(len(tweet_text))+") depassant les "+MAX_TWEET_SIZE+" caracteres",logfile,4)
else:
    logmessage("Longueur du tweet ("+str(len(tweet_text))+")",logfile,1)
    #envoi du tweet
    if(tweet_it):
        try:
            logmessage("Envoi du tweet en réponse au tweet "+reply_to_tweet,logfile,1)
            response = tw_client.create_tweet(text=tweet_text,in_reply_to_tweet_id=reply_to_tweet)
            tweet_id = response.data['id']
            logmessage("Tweet envoyé avec succès. ID : "+tweet_id,logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du tweet : "+format(oops),logfile,4)
    else:
        logmessage("Tweet non envoyé (mode TweetIt inactif)",logfile,3)
        tweet_id = "0000000000000000000"

###
# TWEETS DE REPONSE SUR LES RECORDS
############################################################
logmessage("Préparation des tweets de réponse s'il y a eu des records battus",logfile,1)
reply_to_tweet = tweet_id
#Un record pour la ville?
if(record_ville):
    logmessage("Preparation du tweet pour le record 'ville'",logfile,1)

    #Recherche l'ancien record
    logmessage("Récupération des infos du record précédent",logfile,1)
    query = "SELECT date, SUM(comptage) as total FROM vw_comptages_date where ville = 'Montreuil' GROUP BY date ORDER by total DESC LIMIT 1,1;"
    try:
        previous_record = sql_get_city_recordvilleprecedent("Montreuil")
        previous_record_ville_date      = previous_record['date'][0]
        previous_record_ville_comptages = round(previous_record['total'][0])
        logmessage("Précédent record : "+str(previous_record_ville_comptages)+" le "+datetime.strftime(previous_record_ville_date,"%d/%m/%Y"),logfile,1)
    except Exception as oops:
        logmessage("Echec de la requête: "+format(oops),logfile,4)

    tweet_text = emoji.TADA+"C'est un nouveau record pour Montreuil, qui bat le précédent du "+datetime.strftime(previous_record_ville_date,"%d/%m/%Y")+" et ses "+str(previous_record_ville_comptages)+" passages! "+emoji.TADA

    logmessage("Tweet pour le record de ville: \n\r"+tweet_text,logfile)

    if(len(tweet_text) > MAX_TWEET_SIZE):
        logmessage("Longueur du tweet ("+str(len(tweet_text))+") depassant les "+MAX_TWEET_SIZE+" caracteres",logfile,4)
    else:
        logmessage("Longueur du tweet ("+str(len(tweet_text))+")",logfile,1)
        #envoi du tweet
        if(tweet_it):
            try:
                logmessage("Envoi du tweet en réponse au tweet "+reply_to_tweet,logfile,1)
                response = tw_client.create_tweet(text=tweet_text,in_reply_to_tweet_id=reply_to_tweet)
                tweet_id = response.data['id']
                logmessage("Tweet envoyé avec succès. ID : "+tweet_id,logfile,2)
                reply_to_tweet = tweet_id
            except Exception as oops:
                logmessage("Une erreur s'est produite à l'envoi du tweet : "+format(oops),logfile,4)
        else:
            logmessage("Tweet non envoyé (mode TweetIt inactif)",logfile,3)
            tweet_id = "0000000000000000001"
            reply_to_tweet = tweet_id

#Un record pour un ou plusieurs compteurs?
if(len(daily_records)>0):
    logmessage("Preparation du tweet pour le ou les records 'compteur'",logfile,1)

    #Construction d'un tableau avec les précédents records
    previous_records_compteur = []
    for compteur in daily_records:
        logmessage("Recherche du precedent record pour "+compteur,logfile,1)
        query = "SELECT nom_axe_court, date, SUM(comptage) as total FROM vw_comptages_date where nom_axe_abrg = '"+compteur+"' GROUP BY nom_axe_court,date ORDER by total DESC LIMIT 1,1;"
        try:
            previous_record = sql_get_recordcompteurprecedent(compteur)
            logmessage("Précédent record pour "+previous_record['nom_axe_court'][0]+" : "+str(round(previous_record['total'][0]))+" le "+datetime.strftime(previous_record['date'][0],"%d/%m/%Y"),logfile,1)
            previous_records_compteur.append(previous_record)
        except Exception as oops:
            logmessage("Echec de la requête: "+format(oops),logfile,4)

    #Préparation du texte du tweet

    #Un seul compteur
    if(len(previous_records_compteur)==1):
        nom_compteur = previous_records_compteur[0]['nom_axe_court'][0]
        record = str(round(previous_records_compteur[0]['total'][0]))
        date_record = datetime.strftime(previous_records_compteur[0]['date'][0],"%d/%m/%Y")

        if(record_ville):
            tweet_text = "Et c'est aussi un record pour le compteur "+nom_compteur+" qui bat le précédent du "+date_record+" ("+record+")"+emoji.TADA
        else:
            tweet_text = emoji.TADA+"C'est un nouveau record pour le compteur "+nom_compteur+", qui bat le précédent du "+date_record+" et ses "+record+" passages! "+emoji.TADA
    #Plusieurs compteurs
    else:
        if(record_ville):
            tweet_text = "Et c'est aussi un record pour les compteurs"
        else:
            tweet_text = emoji.TADA+"C'est un nouveau record pour les compteurs"

        logmessage("Liste des noms de compteurs",logfile,1)
        compteurs_list = ""
        i=1
        for p in previous_records_compteur:
            if(i==len(previous_records_compteur)):
                compteurs_list += " et "
            elif(i==1):
                compteurs_list += " "
            else:
                compteurs_list += ", "
            compteurs_list += p['nom_axe_court'][0]
            i+=1
        
        logmessage("   Liste : "+compteurs_list,logfile,1)
        tweet_text += compteurs_list+" qui battent leurs précécents records respectivement des"

        logmessage("Liste des données de compteur",logfile,1)
        compteurs_list = ""
        i=1
        for p in previous_records_compteur:
            if(i==len(previous_records_compteur)):
                compteurs_list += " et "
            elif(i==1):
                compteurs_list += " "
            else:
                compteurs_list += ", "
            compteurs_list += datetime.strftime(p['date'][0],"%d/%m/%Y")+" ("+str(round(p['total'][0]))+")"
            i+=1
        logmessage("   Liste : "+compteurs_list,logfile,1)
        tweet_text += compteurs_list+"!"+emoji.TADA

    logmessage("Tweet : \n\r"+tweet_text,logfile)

    if(len(tweet_text) > MAX_TWEET_SIZE):
        logmessage("Longueur du tweet ("+str(len(tweet_text))+") depassant les "+MAX_TWEET_SIZE+" caracteres",logfile,4)
    else:
        logmessage("Longueur du tweet ("+str(len(tweet_text))+")",logfile,1)
        #envoi du tweet
        if(tweet_it):
            try:
                logmessage("Envoi du tweet en réponse au tweet "+reply_to_tweet,logfile,1)
                response = tw_client.create_tweet(text=tweet_text,in_reply_to_tweet_id=reply_to_tweet)
                tweet_id = response.data['id']
                logmessage("Tweet envoyé avec succès. ID : "+tweet_id,logfile,2)
                reply_to_tweet = tweet_id
            except Exception as oops:
                logmessage("Une erreur s'est produite à l'envoi du tweet : "+format(oops),logfile,4)
        else:
            logmessage("Tweet non envoyé (mode TweetIt inactif)",logfile,3)
            tweet_id = "0000000000000000001"
            reply_to_tweet = tweet_id



###
# SCRIPT ENDING
############################################################

global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,1)
logmessage("Script ending after "+exectimestr,logfile,1)