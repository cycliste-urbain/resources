#!/usr/bin/python3

##############################################
# IMPORTS
import argparse
from datetime import datetime
from os import path
from sqlalchemy import create_engine, text, exc 
from sqlalchemy import text
import pytz
import sys

##############################################
# SETTINGS AND INIT

#Importing config file
import xxxx_stats_config as config
from xxxx_stats_functions import *

set_logpath = config.LOGPATH
USE_SYSLOG = config.USE_SYSLOG

locale_tz = pytz.timezone("Europe/Paris")
logfile = None

#List of supported views to be cached is set in config
SUPPORTED_VIEWS = config.CACHE_SUPPORTED_VIEWS 

##############################################
# FUNCTIONS

def is_naive(d):
    if(d.tzinfo is None or d.tzinfo.utcoffset(d) is None):
        logmessage("Date "+datetime.strftime(d,"%d/%m/%Y %H:%M:%S%z")+" is NAIVE",logfile,1)
    else:
        logmessage("Date "+datetime.strftime(d,"%d/%m/%Y %H:%M:%S%z")+" is AWARE",logfile,1)
    

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Script "+os.path.basename(main.__file__)+" starting up with "+str(len(sys.argv)-1)+" argument(s) "+(' '.join(sys.argv[1:])),None,1,USE_SYSLOG)
logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* xxxx_stats_update_cache.py                                                         *",logfile,2)
logmessage("*    Updates cached MySQL Queries                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1,USE_SYSLOG)

global_chronostart = datetime.now()
##############################################
# ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--views', '-v', help="List of views to be cached", nargs='+', type=str, default="")

args = parser.parse_args()

#Views is provited
if args.views == "":
    #Defaults to all pre-set views
    logmessage("No MySQL views were provided as an argument, defaulting to all pre-set views from config",logfile,3,USE_SYSLOG)
    target_views = SUPPORTED_VIEWS

else:
    #Checks that views are in fact in the supported list
    target_views = []
    for v in args.views :
        if v in SUPPORTED_VIEWS:
            target_views.append(v)
            logmessage("View '"+v+"' will be cached",logfile,1,USE_SYSLOG)
        else:
            logmessage("View '"+v+"' is not listed as supported, it will be skipped",logfile,4,USE_SYSLOG)

logmessage(str(len(target_views))+" MySQL view(s) will be cached",logfile,1,USE_SYSLOG)


#########################################################################################################################################
# CORE PROCESSING  
#########################################################################################################################################

views_count = 0
views_ok   = 0
views_fail = 0

# Loops through views to cache them
for view_name in target_views:
    views_count += 1
    
    # Calculates the cache table name
    cache_table_name = view_name.replace("vw_","cache_")
    cache_table_name = cache_table_name.replace("ctl_","cache_")
 
    logmessage("Caching view '"+view_name+"' into '"+cache_table_name+"'",logfile,1,USE_SYSLOG)    

    # Executing cache query
    mysql_query1  = "TRUNCATE TABLE "+cache_table_name+";"
    mysql_query2  = "INSERT INTO "+cache_table_name+" SELECT * FROM "+view_name+";"

           
    sqlEngine    = create_engine(sql_connect_string_rw, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    chronostart = datetime.now()
            
    try:
        dbConnection.execute(text(mysql_query1))
        dbConnection.commit()
        logmessage("Truncated table '"+cache_table_name+"'",logfile,1,False)
        logmessage("Dumping view into '"+cache_table_name+"'...",logfile,1,False)
        dbConnection.execute(text(mysql_query2))
        dbConnection.commit()
        views_ok += 1
        chronostop = datetime.now()
        chronotime = chronostop-chronostart
        logmessage("Successfully cached view '"+view_name+"' in "+str(round(chronotime.total_seconds(),2))+"s",logfile,2,USE_SYSLOG)

    except Exception as oops:
        views_fail += 1
        logmessage("An error occured during caching MySQL query : "+format(oops),logfile,4,USE_SYSLOG)

    dbConnection.close()
    sqlEngine.dispose()

##############################################
# SCRIPT END


if(views_ok == 0):
    report_level = 4
elif(views_fail > 0):
    report_level = 3
else:
    report_level = 2
global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,report_level)
logmessage("Script ending after "+exectimestr,logfile,report_level,USE_SYSLOG)
logmessage("   Cached data for "+str(views_ok)+"/"+str(views_count)+" views ("+str(views_fail)+" failure(s))",logfile,report_level,USE_SYSLOG)
